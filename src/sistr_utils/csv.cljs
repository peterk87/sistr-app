(ns ^:figwheel-load sistr-utils.csv
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
   [cljs.core.async :refer [put! chan <! timeout]]
   [clojure.string :as string]))

(def quote "\"")
(def sep ",")


(defn read-csv->str
  "Read the text from a JS File using js/FileReader.
  Return a channel."
  [file]
  (let [fr (new js/FileReader)
        c (chan)]
    (go
     (aset fr "onload" #(put! c (.-result (.-target %))))
     (.readAsText fr file))
    c))


(defn read-cell
  "Read a CSV cell from a seq of str.
  Check if CSV cell is quoted."
  [str-seq]
  (loop [c (first str-seq)
         cr (rest str-seq)
         s []
         quoted? false]
    (condp == c
      quote (do
              (recur (first cr) (rest cr) s (not quoted?)))
      sep (if quoted?
            (do
              (recur (first cr) (rest cr) (conj s c) quoted?))
            (if (empty? cr)
              {:cell (apply str s) :rest [""]}
              {:cell (apply str s) :rest cr}))
      (do
        (if (nil? c)
          {:cell (apply str (conj s c)) :rest ()}
          (recur (first cr) (rest cr) (conj s c) quoted?))))))


(defn read-record [record-str]
  (loop [str-seq (array-seq (string/trim record-str))
         out []]
    (let [cell (read-cell str-seq)
          rest-seq (:rest cell)
          out (conj out (:cell cell))]
      (if (empty? rest-seq)
        out
        (recur rest-seq out)))))


(defn parse-csv-text [text]
  (let [lines (string/split-lines (string/trim text))
        header-line (first lines)
        headers (map keyword (read-record header-line))
        data-lines (rest lines)]
    (loop [l (first data-lines)
           lr (rest data-lines)
           data []]
      (if (empty? l)
        data
        (let [record (read-record l)]
          (recur (first lr) (rest lr) (conj data (zipmap headers record))))))))


(defn read-csv!
  "Read and parse a CSV file from js/File into an atom containing list of CSV record maps.
  The first line needs to contain headers.
  All files must have the same number of items/cells.
  The js/File can be retrieved with (first (array-seq @table-file)) if @table-file is js/FileList."
  [csv-file csv-data]
  (go
   (reset! csv-data (parse-csv-text (<! (read-csv->str csv-file))))))
