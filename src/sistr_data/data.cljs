(ns ^:figwheel-load sistr-data.data
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [reagent.core :refer [atom]]
    [sistr-utils.utils :as utils]
    [cljs.core.async :refer [put! chan <! timeout]]
    [ajax.core :refer [GET POST]]
    [clojure.set]))

(def api-uri
  (:api-uri (utils/parse-query-string)
    ;"http://localhost:8000/api/"
    "https://lfz.corefacility.ca/sistr-wtf/api/"
    ))

(def public-sistr-user "sistr")

(defonce serovar-curation-info (atom nil))

(defonce active-page (atom :about-page))
(defonce app-log (atom []))
(defonce http-errors (atom {}))
; try to get username from localStorage, if nil then from URL query string, if nil then set to 'sistr'
(defonce sistr-user (atom (utils/fetch :sistr-user
                                       (:user (utils/parse-query-string) public-sistr-user))))
; watch for changes to username and save to localstorage
(add-watch sistr-user :store #(utils/store :sistr-user %4))
; fetch user auth token from localstorage
(defonce sistr-user-token (atom (utils/fetch :sistr-user-token
                                             (:token (utils/parse-query-string)))))
; save user auth token to localstorage on change
(add-watch sistr-user-token :store #(utils/store :sistr-user-token %4))
; current tree data (Newick tree string; genome groups)
(defonce tree-data (atom nil))
; map of sorted genome names to tree data for those genomes
(defonce tree-data-history (atom {}))
; current MST data (adjacency list; genome groups)
(defonce mst (atom nil))
; map of sorted genomes name to MST data for those genomes
(defonce mst-history (atom {}))
;
(defonce categories (atom []))
; current app-wide genome selection
(defonce genome-selection (atom []))
; map of selection names to genome selection lists
(defonce genome-selection-collection (atom {}))
; save genome selection collection to server on change if possible
(add-watch genome-selection-collection :save
           (fn [_ _ old-state new-state]
             #_(prn "genome-selection-collection" (empty? old-state) (empty? new-state))
             (when-not (or (empty? old-state) (empty? new-state) (nil? @sistr-user-token))
               (go
                 #_(prn "genome old or new not empty!")
                 (let [auth-headers (utils/auth-headers @sistr-user-token)
                       uri (str api-uri "user/" @sistr-user "/selections")
                       resp (<! (utils/put->chan uri
                                                 :data {:selections new-state}
                                                 :headers auth-headers
                                                 :keywords? false))
                       err-resp (:error resp)]
                   (if err-resp
                     (do
                       (utils/log-http app-log :save-user-selections err-resp
                                       :message "Could not save user selections"
                                       :http-method "PUT"
                                       :uri uri))
                     (do
                       (utils/log-http app-log :save-user-selections resp
                                       :message "User genome selections saved to DB!"
                                       :http-method "PUT"
                                       :uri uri)
                       ;TODO: overwrite genome selections with response from server?
                       )))))))

; map of genome names to genome metadata
(defonce genome-name-metadata (atom nil))

;; TODO: ask server for default columns
(defonce default-cols (atom [:genome
                             :user_uploader
                             :serovar
                             :serovar_predicted
                             :MLST_ST_in_silico
                             :source_type
                             :source_info
                             :host_common_name
                             :host_latin_name
                             :source_country
                             :source_region
                             :collection_year
                             :collection_month
                             :collection_date
                             :subspecies
                             :cgMLST_cluster_100%
                             :cgMLST_cluster_99%
                             :cgMLST_cluster_95%
                             :cgMLST_cluster_90%
                             :cgMLST_cluster_80%
                             :cgMLST_cluster_70%
                             :cgMLST_cluster_10%
                             :curation_info
                             ]))


(defonce serovar-prediction-results (atom nil))
;; TODO: ask server for serovar-prediction-fields including order, description, etc
(def serovar-prediction-fields [:genome
                                :user_uploader
                                :subspecies
                                :curated_serovar
                                :serovar_prediction
                                :serovar_antigen_prediction
                                :Serogroup_prediction
                                :H1_prediction
                                :H2_prediction
                                :cgMLST_serovar_prediction
                                :cgMLST_serovar_count_predictions
                                :cgMLST_cluster_level
                                :MLST_ST_in_silico
                                :MLST_serovar_prediction
                                :MLST_serovar_count_predictions
                                :curation_info
                                :reported_serovar])

(defonce svg-data (atom ""))

;; In order to refresh the app data when a user uploads genomes, the `upload-tracker` integer
;; atom is used to determine when to fetch new data.
;; When the user uploads some genomes to the DB then `upload-tracker` is set to the number of
;; genomes being uploaded.
(defonce upload-countdown (atom 0))

(defn get-phylo->chan
  "Retrieve the `tree|mst` (tree or MST) data subset for specified `genomes`.
  `tree|mst` must be 'newick' or 'mst'
  If `genomes` is empty then the entire subset of tree or MST data is retrieved."
  [tree|mst & {:keys [genomes headers] :or {genomes [] headers {}}}]
  (let [c (chan)
        uri (str api-uri "user/" @sistr-user "/genomes/wgmlst/" tree|mst)
        args {:response-format :json
              :keywords?       true
              :format          :json
              :handler         #(put! c (assoc % :uri uri))
              :error-handler   #(put! c {:error (assoc % :uri uri)})
              :headers         headers}]
    (POST uri
          (if (empty? genomes)
            args
            (assoc args :params {:genome genomes})))
    c))

(defn get-serovar-curation-info! []
  (go
    (let [uri (str api-uri "curation_info")
          resp (<! (utils/get-uri->chan uri))
          error-resp (:error resp)]
      (if (nil? error-resp)
        (do
          (reset! serovar-curation-info (utils/map-lists->list-maps resp))
          (utils/log-http app-log :api resp
                          :uri uri
                          :message "Retrieved serovar prediction curation info!"))
        (do
          (utils/log-http-error http-errors :api error-resp :uri uri)
          (utils/log-http app-log :api error-resp
                          :uri uri
                          :message "Unable to retrieve serovar prediction curation info."))))))


(defn authorization-headers-for-current-user []
  (let [auth-chan (chan)]
    (go
      ;; try getting username from query string
      (when (= @sistr-user public-sistr-user)
        (reset! sistr-user (utils/get-user-from-query-string public-sistr-user)))
      ;; try to renew token if it exists, otherwise, reset token to nil and username to default
      (when @sistr-user-token
        (let [token-uri (str api-uri "token")
              token-resp (<! (utils/get-auth-token token-uri @sistr-user-token))
              token-error (:error token-resp)
              err-msg "Error occurred trying to renew authorization token. Please log in again."]
          (if token-error
            (do
              (utils/log-http-error http-errors
                                    :token token-error
                                    :message err-msg
                                    :uri token-uri)
              (utils/log-http app-log :token token-error
                              :message err-msg
                              :uri token-uri)
              (reset! sistr-user-token nil)
              (reset! sistr-user public-sistr-user))
            (do
              (swap! http-errors dissoc :token)
              (utils/log-http app-log :token token-resp
                              :message (str "Retrieved token for current user '" @sistr-user "'!")
                              :uri token-uri)
              (reset! sistr-user-token token-resp)
              ))))
      ;; try to retrieve user info with renewed token
      (when @sistr-user-token
        (let [auth-header (utils/auth-headers @sistr-user-token)
              uri (str api-uri "user/" @sistr-user)
              resp (<! (utils/get-uri->chan uri
                                            :headers auth-header))
              resp-error (:error resp)]
          (if resp-error
            (do
              (let [err-msg (str "Error occurred trying to access info for user "
                                 @sistr-user
                                 " with renewed auth token. Please log in again.")]
                (utils/log-http-error http-errors
                                      :token resp-error
                                      :uri uri
                                      :message err-msg)
                (utils/log-http app-log :token resp-error
                                :message err-msg
                                :uri uri)
                (reset! sistr-user-token nil)
                (reset! sistr-user public-sistr-user)))
            (do
              (utils/log-http app-log :token resp
                              :message "Retrieved user info with updated token"
                              :uri uri)
              (swap! http-errors dissoc :token)
              (put! auth-chan auth-header)))))
      (when-not @sistr-user-token
        (put! auth-chan {})))
    auth-chan))


(defn get-app-data!
  "Get the basic data for the app
  - get user (update time last seen) or create if new user specified
  - genome metadata
  - in silico typing test results
  - MST data
  - phylogenetic tree Newick data
  "
  []
  #_(js/console.log "Retrieving app data from server")
  (get-serovar-curation-info!)
  #_(js/console.log "Retrieving genome metadata")
  (go
    (let [auth-header (<! (authorization-headers-for-current-user))
          md-uri (str api-uri "user/" @sistr-user "/genomes/metadata")
          md-resp (<! (utils/get-uri->chan md-uri :headers auth-header))
          md-resp-error (:error md-resp)]
      (if md-resp-error
        (do
          (let [err-msg (str "Unable to get user '" @sistr-user "' genomes metadata.")]
            (utils/log-http-error http-errors :genome-metadata md-resp-error
                                  :message err-msg
                                  :uri md-uri)
            (utils/log-http app-log :genome-metadata md-resp-error
                            :message err-msg
                            :uri md-uri)))
        (do
          (swap! http-errors dissoc :genome-metadata)
          (utils/log-http app-log :genome-metadata md-resp
                          :message (str "Retrieved metadata for genomes accessible to user '" @sistr-user "'")
                          :uri md-uri)
          (let [md (->> md-resp
                        (utils/map-lists->list-maps)
                        (utils/remove-map-nil-vals))
                names (map #(get % :genome) md)
                map-names-md (zipmap names md)]
            (reset! genome-name-metadata map-names-md)
            (reset! categories (->> md first keys (concat @default-cols) distinct))
            (reset! default-cols @categories)
            ; TODO: add user genomes to selection
            (let [genomes-big-clusters (->> @genome-name-metadata vals
                                            (group-by :cgMLST_cluster_100%)
                                            (filter (fn [[_ v]] (> (count v) 1)))
                                            vals
                                            flatten
                                            (map :genome)
                                            sort)
                  selection-name "cgMLST 100% clusters with size >= 1"]
              (swap! genome-selection-collection assoc selection-name genomes-big-clusters)
              (let [mst-resp (<! (get-phylo->chan "mst"
                                                  :genomes genomes-big-clusters
                                                  :headers auth-header))
                    mst-resp-error (:error mst-resp)]
                (if mst-resp-error
                  (do
                    (let [err-msg (str "Could not fetch MST for " (count genomes-big-clusters)
                                       " genomes for user '" @sistr-user "'.")]
                      (utils/log-http-error http-errors :mst mst-resp-error
                                            :uri (:uri mst-resp-error)
                                            :message err-msg)
                      (utils/log-http app-log :mst mst-resp-error
                                      :message err-msg
                                      :uri (:uri mst-resp-error))))
                  (do
                    (swap! http-errors dissoc :mst)
                    (utils/log-http app-log :mst mst-resp
                                    :message (str "Retrieved cgMLST-based MST for genomes belonging to " selection-name)
                                    :uri (:uri mst-resp))
                    (reset! mst mst-resp)
                    (swap! mst-history assoc genomes-big-clusters mst-resp))))
              (let [tree-resp (<! (get-phylo->chan "newick"
                                                   :genomes genomes-big-clusters
                                                   :headers auth-header))
                    tree-resp-error (:error tree-resp)]
                (if tree-resp-error
                  (do
                    (let [err-msg (str "Unable to retrieve cgMLST tree for user '" @sistr-user "'")]
                      (utils/log-http-error http-errors :tree tree-resp-error
                                            :message err-msg
                                            :uri (:uri tree-resp-error))
                      (utils/log-http app-log :tree tree-resp-error
                                      :message err-msg
                                      :uri (:uri tree-resp-error))))
                  (do
                    (swap! http-errors dissoc :tree)
                    (utils/log-http app-log :tree tree-resp
                                    :message (str "Retrieved cgMLST tree for genomes belonging to " selection-name)
                                    :uri (:uri tree-resp))
                    (reset! tree-data tree-resp)
                    (let [sorted-genome-names (->> @tree-data :genomes concat flatten sort)]
                      (swap! tree-data-history assoc sorted-genome-names @tree-data)))))))))))
  (go
    (let [uri (str api-uri "queue")]
      (let [resp (<! (utils/get-uri->chan uri))
            err-resp (:error resp)]
        (when (nil? err-resp)
          (reset! sistr-app.pages.upload/queue-status (assoc resp :timestamp (js/Date.)))))))
  (go
    (let [auth-header (<! (authorization-headers-for-current-user))
          uri (str api-uri "user/" @sistr-user "/selections")
          resp (<! (utils/get-uri->chan uri :headers auth-header :keywords? false))
          err-resp (:error resp)]
      (if err-resp
        (do
          (let [err-msg "Unable to retrieve user genome selections"]
            (utils/log-http-error http-errors :user-genome-selections err-resp
                                  :message err-msg
                                  :uri uri)
            (utils/log-http app-log :user-genome-selections err-resp
                            :message err-msg
                            :uri uri)))
        (do
          (utils/log-http app-log :user-genome-selections resp
                          :message "Retrieved user genome selections!"
                          :uri uri)
          (swap! genome-selection-collection merge resp)))))
  (go
    (let [public-sp-uri (str api-uri "user/" public-sistr-user "/genomes/serovar_prediction")
          public-sp-resp (<! (utils/get-uri->chan public-sp-uri))
          public-sp-resp-error (:error public-sp-resp)]
      (if public-sp-resp-error
        (do
          (let [err-msg (str "Unable to retrieve public genome serovar prediction results")]
            (utils/log-http-error http-errors :serovar_prediction public-sp-resp-error
                                  :message err-msg
                                  :uri public-sp-uri)
            (utils/log-http app-log :serovar_prediction public-sp-resp-error
                            :message err-msg
                            :uri public-sp-uri))
          )
        (do
          (let [public-serovar-predictions (->> public-sp-resp
                                                (utils/map-lists->list-maps)
                                                (utils/remove-map-nil-vals))]
            (if (= @sistr-user public-sistr-user)
              (do
                (swap! http-errors dissoc :serovar_prediction)
                (utils/log-http app-log :serovar_prediction public-sp-resp
                                :message "Retrieved serovar prediction results for public genomes."
                                :uri public-sp-uri)
                (reset! serovar-prediction-results public-serovar-predictions))
              (let [auth-header (<! (authorization-headers-for-current-user))
                    uri (str api-uri "user/" @sistr-user "/genomes/serovar_prediction")
                    sp-resp (<! (utils/get-uri->chan uri
                                                     :headers auth-header))
                    sp-resp-error (:error sp-resp)]
                (if sp-resp-error
                  (do
                    (let [err-msg (str "Unable to retrieve serovar prediction results for user '" @sistr-user "'!")]
                      (utils/log-http-error http-errors :serovar_prediction sp-resp-error
                                            :message err-msg
                                            :uri uri)
                      (utils/log-http app-log :serovar_prediction sp-resp-error
                                      :message (str err-msg " Setting serovar prediction results data to public only.")
                                      :uri uri)
                      (reset! serovar-prediction-results public-serovar-predictions)))
                  (do
                    (let [serovar-predictions (->> sp-resp
                                                   (utils/map-lists->list-maps)
                                                   (utils/remove-map-nil-vals))]
                      (swap! http-errors dissoc :serovar_prediction)
                      (utils/log-http app-log :serovar_prediction sp-resp
                                      :message (str "Retrieved serovar predictions for user '" @sistr-user "' genomes!")
                                      :uri uri)
                      ; merge serovar prediction results for public and private user genomes
                      (reset! serovar-prediction-results (flatten
                                                           (concat
                                                             serovar-predictions
                                                             public-serovar-predictions))))))))))))))

(defn genome-groups-list->map
  "Convert genome groups lists to map with first genome in each group list as
  the key and the rest as the values.
  `mst` and `newick` atoms have grouped genome lists under :genomes"
  [genomes-lists]
  (let [parent-genomes (map first genomes-lists)
        child-genomes (map rest genomes-lists)]
    (zipmap parent-genomes child-genomes)))

(defn logout!
  "Reset username to `sistr` and auth token to nil, clear query string of `?user=<username>` and get default app data."
  []
  (reset! sistr-user public-sistr-user)
  (reset! sistr-user-token nil)
  (utils/clear-query-string!)
  (get-app-data!))

(defn logged-in?
  "User is logged if username is not `sistr` or if username is `sistr` and has valid auth token."
  [user token]
  (or (and (= user public-sistr-user)
           token)
      (not= user public-sistr-user)))


(add-watch upload-countdown :done?
           (fn [_ _ old-val new-val]
             (when (and (zero? new-val) (> old-val 0))
               (get-app-data!))))