(ns ^:figwheel-load sistr-components.dimple-charts
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
    [cljsjs.dimple]
    [reagent.core :as r :refer [atom]]
    [sistr-utils.utils :as utils]
    [sistr-components.selectize :refer [single-selectize multi-selectize]]
    [sistr-components.bootstrap :refer [panel]]))

(def missing-value "<NA>")


(defn horiz-bar-chart [id data opts]
  (let [chart-id (str id "-dimple-horiz-bar-chart")]
    (letfn [(render! []
                     (deref data)
                     (deref opts)
                     [:div#dimple-chart {:id    chart-id
                                         :style {:width  "100%"
                                                 :height 600}}])
            (mounter! []
                      (let [{:keys [category-key
                                    series-key
                                    limit
                                    offset
                                    on-click-handler!
                                    log-count]} @opts
                            _ (set! (.-innerHTML (utils/by-id chart-id)) "")
                            svg (js/dimple.newSvg (str "#" chart-id) "100%" "100%")
                            chart-data (clj->js (->> (if (map? @data)
                                                       (vals @data)
                                                       @data)
                                                     (group-by #(get % category-key))
                                                     (remove (fn [[k _]] (nil? k)))
                                                     (sort-by (fn [[_ v]] (count v)))
                                                     reverse
                                                     (take limit)
                                                     (drop offset)
                                                     vals
                                                     flatten
                                                     (map (fn [x]
                                                            (let [category (get x category-key)
                                                                  series (get x series-key)]
                                                              [(if (nil? category)
                                                                 missing-value
                                                                 category)
                                                               (if (nil? series)
                                                                 missing-value
                                                                 series)])))
                                                     frequencies
                                                     (sort-by (fn [[_ x]] x))
                                                     reverse
                                                     (map (fn [[[x y] count]]
                                                            {category-key x
                                                             series-key   y
                                                             :count       count}))))
                            dimple-chart (new js/dimple.chart svg chart-data)]
                        (if log-count
                          (.. dimple-chart (addLogAxis "x" "count"))
                          (.. dimple-chart (addMeasureAxis "x" "count")))
                        (.. dimple-chart (addCategoryAxis "y" (utils/keyword->str category-key)))
                        (let [series (.. dimple-chart
                                         (addSeries
                                           (utils/keyword->str series-key)
                                           js/dimple.plot.bar))]
                          (when-not (nil? on-click-handler!)
                            (.. series (addEventHandler
                                         "click"
                                         (fn [e]
                                           (let [y (.. e -yValue)
                                                 x (first (js->clj (.. e -seriesValue)))
                                                 ]
                                             (on-click-handler! (if (= y missing-value) nil y)
                                                                (if (= x missing-value) nil x)
                                                                category-key
                                                                series-key)))))))
                        (.. dimple-chart (addLegend "10%,20px" 10 "80%" "10%" "right"))
                        (.. dimple-chart (draw))))]
      (r/create-class {:render               render!
                       :component-did-mount  mounter!
                       :component-did-update mounter!}))))


(defn int-input-box [id label value & {:keys [threshold] :or {threshold 0}}]
  #_(prn "int input box" id label value)
  [:div {:id (str id "-container")}
   [:label {:for id} label]
   [:div.input-group
    [:input.form-control {:id        id
                          :name      id
                          :type      "numeric"
                          :value     @value
                          :on-change (fn [e]
                                       (let [x (int (-> e .-target .-value))]
                                         (when (and x (>= x threshold))
                                           (reset! value x))))}]
    [:div.input-group-btn
     [:button.btn.btn-default {:on-click #(swap! value inc)} [:i.fa.fa-plus]]
     [:button.btn.btn-default {:on-click #(when (> @value threshold)
                                           (swap! value dec))} [:i.fa.fa-minus]]]]])


(defn checkbox-input [id label value]
  [:label
   [:input {:type      "checkbox"
            :id        id
            :checked   @value
            :on-change #(reset! value (.-checked (.-target %)))}]
   label])


(defn horiz-bar-chart-controls [id categories opts]
  [:div
   [:div.row
    [:div.col-md-6
     [single-selectize
      (str id "-category-select")
      "Chart Y Category"
      categories
      (r/cursor opts [:category-key])
      #(swap! opts assoc :category-key (keyword (js->clj %)))]
     ]
    [:div.col-md-6
     [int-input-box "dimple-n-take" "Y-category show limit" (r/cursor opts [:limit])]
     ]
    ]
   [:div.row
    [:div.col-md-6
     [single-selectize
      (str id "-series-select")
      "Chart Series Category"
      categories
      (r/cursor opts [:series-key])
      #(swap! opts assoc :series-key (keyword (js->clj %)))]]
    [:div.col-md-6
     [int-input-box "dimple-n-drop" "Y-category show offset" (r/cursor opts [:offset])]]]
   [:div.row
    [:div.col-md-4
     [checkbox-input
      (str id "-log-count")
      " Log Scale X-axis"
      (r/cursor opts [:log-count])]]]])


(defn horiz-bar-chart-panel
  [id title data categories &
   {:keys [category-key
           series-key
           limit
           offset
           on-click-handler!
           log-count]
    :or   {limit             10
           offset            0
           category-key      nil
           series-key        nil
           on-click-handler! nil
           log-count         false}}]
  (let [opts (atom {:limit             limit
                    :offset            offset
                    :category-key      category-key
                    :series-key        series-key
                    :on-click-handler! on-click-handler!
                    :log-count         log-count})]
    #_(prn "dimple panel" opts)
    (fn []
      [panel (str id "-panel")
       title
       "default"
       false
       [:div
        [horiz-bar-chart-controls id categories opts]
        [horiz-bar-chart id data opts]]
       ])))
