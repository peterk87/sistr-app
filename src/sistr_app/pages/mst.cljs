(ns ^:figwheel-load sistr-app.pages.mst
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljs.core.async :refer [put! chan <! timeout]]
    [reagent.core :as reagent :refer [atom]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [sistr-components.alerts :refer [alert-dismissable]]
    [sistr-components.slickgrid :refer [slick-grid-table]]
    [sistr-components.selectize :refer [single-selectize multi-selectize]]
    [sistr-components.bootstrap :as bootstrap :refer [well-collapsible]]
    [sistr-components.selection-manager :as selection]))

;; Minimum spanning tree (MST)
;; An MST adjacency graph for a wgMLST330 distance matrix is generated on the server
;; and sent to the client where it is rendered using a custom D3 MST class using a
;; force-directed graph visualization.
;; Users can:
;; - highlight metadata onto the MST visualization
;; - select nodes for which to display metadata in a table below the graph
;; - save an SVG of the graph

(def show-mst? (atom false))

(defonce mst-js-obj (atom nil))

(defonce category (atom "serovar_predicted"))
(defonce category-group-items (atom []))
(defonce groups (atom []))

(defonce selected-mst-genomes (atom []))
(defonce selected-metadata (atom nil))

(defonce selection-table-grid (atom nil))
(defonce selection-table-dataview (atom nil))

(defn d3-mst-component []
  (deref data/mst)
  (deref show-mst?)
  [:div {:id "d3-mst"}])

(defn group-changer! []
  (let [genomes (if (nil? @data/mst)
                  (keys @data/genome-name-metadata)
                  (->> (:genomes @data/mst) flatten))
        groups (->> genomes
                    (map #(get @data/genome-name-metadata %))
                    vec
                    (map #(get % (keyword @category))))]
    (reset! category-group-items groups)))

(defn mst-change-watcher []
  (let [changed (utils/listen (utils/by-id "d3-mst") "change")]
    (go (while true
          (<! changed)
          (reset! selected-mst-genomes (js->clj (.-selected @mst-js-obj)))
          (let [genome-list (:genomes @data/mst)
                parent-genomes (->> genome-list (map first))
                child-genomes (->> genome-list (map rest))
                grouped-genomes (zipmap parent-genomes child-genomes)
                sel (flatten (map #(concat [%] (get grouped-genomes %)) @selected-mst-genomes))]
            (reset! selected-metadata (map #(get @data/genome-name-metadata %) sel)))
          ))))

(defn d3-mst-render! []
  (when (and @show-mst? (not (nil? @data/mst)))
    (group-changer!)
    (let [nodes (->> @data/mst (:genomes) (map first) (clj->js))
          links (->> @data/mst
                     (:mst)
                     (:links)
                     (map #(let [{:keys [target source weight]} %]
                            {:source    source
                             :target    target
                             :distances (utils/round (* 100 weight) 1)}))
                     (vec)
                     (clj->js))
          genome-list (:genomes @data/mst)
          parent-genomes (->> genome-list (map first))
          child-genomes (->> genome-list (map rest))
          grouped-genomes (zipmap parent-genomes child-genomes)
          data {:md             @data/genome-name-metadata
                :show_distances true
                :grouped_nodes  grouped-genomes
                :max_distance   (->> @data/mst (:mst) (:links) (map :weight) (map #(* 100 %)) (apply max))}]
      (when (nil? @mst-js-obj)
        (reset! mst-js-obj (new js/MST (utils/by-id "d3-mst"))))
      (.update_nodes @mst-js-obj nodes)
      (.update_links @mst-js-obj links nodes)
      (.update @mst-js-obj (clj->js data))
      (let [n-groups (count @groups)
            colours (utils/qual-colours n-groups)]
        (.update_highlighted_groups @mst-js-obj
                                    (clj->js @category)
                                    (clj->js (map utils/keyword->str @groups))
                                    (clj->js colours)))
      (.show_node_sizes @mst-js-obj false)
      (.show_link_distances @mst-js-obj false)
      ;; add watcher for changes in MST selection
      (mst-change-watcher)
      )))

(defn d3-mst []
  (reagent/create-class
    {:render               d3-mst-component
     :component-did-mount  d3-mst-render!
     :component-did-update d3-mst-render!
     }))

(defn category-select-handler! [e]
  (reset! category (js->clj e))
  (group-changer!)
  )

(defn mst-category-select []
  (single-selectize
    "mst-category"
    "Select category"
    data/categories
    category
    category-select-handler!))

(defn mst-update-group-highlighting! []
  (when-not (nil? @mst-js-obj)
    (let [n-groups (count @groups)
          qual-colours (utils/qual-colours n-groups)]
      (.update_highlighted_groups @mst-js-obj
                                  (clj->js @category)
                                  (clj->js (map utils/keyword->str @groups))
                                  (clj->js qual-colours)))))

(defn mst-group-change-handler! [e]
  (when-not (and (nil? @data/mst) (nil? @data/genome-name-metadata))
    (reset! groups (map keyword (js->clj e)))
    (mst-update-group-highlighting!)
    ))

(defn mst-groups-select []
  (group-changer!)
  (multi-selectize
    "mst-groups"
    "Select groups"
    category-group-items
    groups
    mst-group-change-handler!))

(defn mst-group-highlight
  "Create 2 Selectize widgets for controlling what groups to highlight in the
  MST plot from which category."
  []
  (deref data/categories)
  (deref data/genome-name-metadata)
  (deref mst-js-obj)
  [:div
   [mst-category-select]
   [mst-groups-select]])

(defn selection-table-header []
  (when-not (empty? @selected-mst-genomes)
    (let [n-nodes (count @selected-mst-genomes)
          n-genomes (count @selected-metadata)
          node-label (str n-nodes " " (utils/pluralize "node" n-nodes))
          genome-label (str n-genomes " " (utils/pluralize "genome" n-genomes))]
      [:div
       [:h2 (str "MST selection table for " genome-label " from " node-label)]])))

(defn selection-table []
  (when-not (empty? @selected-mst-genomes)
    [slick-grid-table
     "mst-selection-table"
     selected-metadata
     :genome
     (map utils/keyword->str @data/default-cols)
     (atom [])
     selection-table-grid
     selection-table-dataview]))

(defn download-mst-table []
  (when-not (empty? @selected-mst-genomes)
    ;; get the selected genomes from the MST node selection
    (let [n-selected (count @selected-metadata)
          ;; get all metadata headers
          header-kws (->> @selected-metadata
                          first
                          keys
                          sort)
          ;; get CSV headers ordered by selected columns with the rest of the sorted columns appended
          ordered-header-kws (distinct (concat @data/default-cols header-kws))
          csv-data (utils/map->csv-text @selected-metadata ordered-header-kws)
          url-obj (utils/text->blob->obj-url csv-data)]
      ;; this component with the proper CSV data
      [:a.btn.btn-default {:href     url-obj
                           :download (str "subset-" n-selected (utils/pluralize "genome" n-selected) "-" (count @selected-mst-genomes) "table.csv")}
       [:i.fa.fa-download.fa-lg.pull-left]
       " Download Table (CSV)"
       [:br]
       (str n-selected " selected " (utils/pluralize "genome" n-selected))])))

(defn clear-mst-genome-selection-btn []
  (when-not (empty? @selected-mst-genomes)
    [:button.btn.btn-danger {:on-click #(.clear_selection @mst-js-obj)
                             :style    {:margin-bottom 10}}
     [:i.fa.fa-ban] " Clear selection "
     (let [n (count @selected-mst-genomes)
           label (str n " " (utils/pluralize "node" n))]
       [:span.badge label])]))

(defn mst-loading-indicator []
  (when (empty? @data/mst)
    [:div.alert.alert-warning
     [:h5 [:i.fa.fa-spinner.fa-spin.fa-lg] " Loading MST data..."]]))

(defn mst-help-info []
  [:div
   [:div.panel.panel-default
    [:div.panel-heading "MST plot interactions:"]
    [:div.panel-body
     [:p [:i.fa.fa-search-plus] "/" [:i.fa.fa-search-minus] [:strong " Zoom in and out "] "by scrolling your mousewheel"]
     [:p [:i.fa.fa-arrows] [:strong " Pan "] "by clicking and dragging within the plot area"]
     [:p [:i.fa.fa-crosshairs] [:strong " Click "] "a node to select/deselect genomes within node for displaying metadata in table below plot"]
     [:p [:i.fa.fa-crosshairs] "+" [:i.fa.fa-arrows] [:strong " Click and drag "] "a node fix its position"]
     [:p [:i.fa.fa-crosshairs] [:i.fa.fa-crosshairs] [:strong " Double click "] "a \"fixed\" node to \"unfix\" it"]
     ]]
   [:div.panel.panel-default
    [:div.panel-heading "Highlighting metadata in MST plot:"]
    [:div.panel-body
     [:p "Select a category to highlight groups from in the MST"]
     [:p "Select one or more groups from that category"]
     [:p "Click in the group selection box and select a group"]
     [:p "Type to filter the available selections!"]]]])

(defn mst-controls []
  (when-not (empty? @data/mst)
    [:form {:role "form"}
     [:div.form-group
      [:label
       [:input {:type      "checkbox"
                :id        "show-node-size"
                :on-change #(.show_node_sizes @mst-js-obj (.-checked (.-target %)))}]
       " Show node size numbers"]
      [:label
       [:input {:type      "checkbox"
                :id        "show-distance-numbers"
                :on-change #(.show_link_distances @mst-js-obj (.-checked (.-target %)))}]
       " Show distances"]
      [:label
       [:input {:type      "checkbox"
                :id        "sticky-nodes"
                :on-change #(.enable_sticky_nodes @mst-js-obj (.-checked (.-target %)))}]
       " Enable node \"fixing\" in place"]
      [:button.btn.btn-default {:type     "button"
                                :on-click #(.fix_all_nodes @mst-js-obj)} "Fixate all nodes"]]]))

(defn mst-refresh! []
  (if (nil? @mst-js-obj)
    (d3-mst-render!)
    (do
      (._update_svg_size @mst-js-obj)
      (mst-update-group-highlighting!))))

(defn mst-info []
  (when-not (or (nil? @mst-js-obj) (nil? @data/mst))
    (let [n-nodes (count (:genomes @data/mst))
          n-genomes (->> @data/mst :genomes flatten count)]
      [:div.alert.alert-info
       (str "Showing " n-genomes " " (utils/pluralize "genome" n-genomes)
            " in " n-nodes " " (utils/pluralize "node" n-nodes))])))


(defn view-history-item [genomes mst-data]
  ^{:key [genomes mst-data]}
  [:tr {:style {:background-color (if (= mst-data @data/mst) "rgba(0,100,255,0.2)" "transparent")}}
   (let [sel-hist-item (ffirst (filter (fn [[_ v]] (= genomes (sort v))) @data/genome-selection-collection))]
     [:td {:on-click #(do
                       #_(prn "Clicked " genomes (count (:genomes mst-data)))
                       (reset! data/mst mst-data))
           :style    {:cursor "pointer"}}
      (when-not (nil? sel-hist-item)
        [:strong (str "[" sel-hist-item "] - ")])
      (str (->> mst-data :genomes count) " node(s); " (->> mst-data :genomes flatten count) " genome(s)")])
   [:td.pull-right
    [:button.destroy {:type     "button"
                      :on-click #(swap! data/mst-history dissoc genomes)}
     [:i.fa.fa-close.text-danger.fa-lg]]]])


(defn view-history-table []
  [:table.table.table-condensed
   [:tbody
    (map-indexed
      (fn [i [genomes mst-data]]
        ^{:key i}
        [view-history-item genomes mst-data])
      @data/mst-history)]])


(defn btns-show-selected-genomes []
  (let [curr-tree-genomes (->> @data/mst :genomes flatten sort)
        sorted-genome-selection (->> @data/genome-selection sort)]
    [:div
     (when-not (or (empty? sorted-genome-selection)
                   (= sorted-genome-selection curr-tree-genomes))
       [:button.btn.btn-danger
        {:on-click (fn [_]
                     (let [existing-mst (get @data/mst-history sorted-genome-selection)]
                       (if (nil? existing-mst)
                         (do
                           (reset! data/mst nil)
                           (go
                             (let [auth-header (<! (data/authorization-headers-for-current-user))
                                   resp (<! (data/get-phylo->chan "mst"
                                                                  :genomes sorted-genome-selection
                                                                  :headers auth-header))
                                   err-resp (:error resp)]
                               (if err-resp
                                 (do
                                   (utils/log-http data/app-log :tree err-resp
                                                   :message "Could not get MST"
                                                   :http-method "POST"
                                                   :uri "/api/mst"))
                                 (do
                                   (utils/log-http data/app-log :tree err-resp
                                                   :message "Retrieved MST!"
                                                   :http-method "POST"
                                                   :uri "/api/mst")
                                   (reset! data/mst resp)
                                   (when-not (or (nil? @data/mst)
                                                 (some #(= @data/mst %) @data/mst-history))
                                     (swap! data/mst-history assoc sorted-genome-selection @data/mst)))))))
                         (do
                           (reset! data/mst existing-mst)))
                       ))}
        (str "Show only " (count sorted-genome-selection) " genomes")])]))


(defn reset-mst-graph! []
  (reset! show-mst? true)
  (reset! mst-js-obj nil)
  (d3-mst-render!))


(defn btn-graph-reset []
  [:button.btn.btn-default {:on-click #(reset-mst-graph!)}
   [:i.fa.fa-refresh] " Refresh MST"])

(defn view-history-panel []
  [well-collapsible "tree-view-history-panel"
   [:span [:i.fa.fa-tree] " View Manager"]
   false
   [:div
    [:p [:strong "View History"]]
    [view-history-table]
    [btns-show-selected-genomes]
    [btn-graph-reset]]])

(defn mst-page []
  [:div {:style {:padding-left 300}}
   [:div.sidebar
    [:div.well.well-sm
     [mst-loading-indicator]
     [well-collapsible "mst-help-info"
      [:span [:i.fa.fa-info-circle] " Help"]
      true
      [mst-help-info]]
     [mst-group-highlight]
     [mst-controls]
     [selection/genome-selection-manager "mst-selection-manager" #(prn "mst-selection-manager on-change event")]
     [view-history-panel]]]
   [:div.container-fluid
    [:div.row
     [:div.col-md-12
      [d3-mst]
      [:div
       [bootstrap/download-svg-button "Download MST as SVG" "d3-mst" data/svg-data]
       [clear-mst-genome-selection-btn]
       [download-mst-table]
       ]]]
    [:div.row
     [selection-table-header]
     [selection-table]]]])
