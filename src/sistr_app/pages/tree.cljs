(ns ^:figwheel-load sistr-app.pages.tree
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljsjs.dimple]
    [cljs.core.async :refer [put! chan <! timeout]]
    [reagent.core :as reagent :refer [atom]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [sistr-components.alerts :refer [alert-dismissable]]
    [sistr-components.slickgrid :refer [slick-grid-table]]
    [sistr-components.selectize :refer [single-selectize multi-selectize]]
    [sistr-components.bootstrap :as bootstrap :refer [well-collapsible]]
    [sistr-components.selection-manager :as selection]))

(defonce show-tree? (atom false))
(defonce phylocanvas-js-obj (atom nil))
(defonce category (atom "serovar_predicted"))
(defonce groups (atom []))
(defonce category-group-items (atom []))
(defonce selected-nodes (atom nil))
(defonce selected-metadata (atom nil))
(defonce table-grid (atom nil))
(defonce table-dataview (atom nil))

(defn group-changer! []
  (let [genomes (if (nil? @data/tree-data)
                  (keys @data/genome-name-metadata)
                  (->> (:genomes @data/tree-data) flatten))
        groups (->> genomes
                    (map #(get @data/genome-name-metadata %))
                    vec
                    (map #(get % (keyword @category))))]
    (reset! category-group-items groups)))

(defn category-change-handler! [e]
  (reset! category (js->clj e))
  (group-changer!))

(defn category-select []
  (when-not (and (empty? @data/categories) (nil? @data/tree-data))
    (single-selectize
      "tree-category"
      "Select category to highlight"
      data/categories
      category
      category-change-handler!)))

(defn set-phylocanvas-metadata! []
  (when-not (nil? @phylocanvas-js-obj)
    (let [n-groups (count @groups)
          qual-colours (utils/qual-colours n-groups)
          groups-to-colours (zipmap @groups qual-colours)
          ]
      #_(prn "set-phylocanvas-metadata!" n-groups qual-colours)
      (let [genomes (:genomes @data/tree-data)
            gs (map first genomes)
            md (map (fn [xs]
                      (->> xs
                           (map (fn [x]
                                  (get-in @data/genome-name-metadata [x (keyword @category)])))
                           (map str)
                           frequencies)
                      )
                    genomes)
            md-col (map (fn [freqs]
                          (let [sum-freqs (reduce + (vals freqs))
                                in-groups (->> @groups
                                               (map (fn [g]
                                                      (let [freq (get freqs g)]
                                                        {:color (get groups-to-colours g)
                                                         :count (if freq freq 0)})))
                                               (remove (fn [{:keys [count]}] (zero? count)))
                                               )

                                sum-in-groups (reduce + (map :count in-groups))]
                            (if (= sum-freqs sum-in-groups)
                              in-groups
                              (conj in-groups {:color "#aaa" :count (- sum-freqs sum-in-groups)}))
                            )
                          )
                        md)]
        (.setMetadata @phylocanvas-js-obj (clj->js (zipmap gs md-col)))
        (.colorBranchesByLeafMetadata @phylocanvas-js-obj)
        (.draw @phylocanvas-js-obj)))))

(defn group-change-handler! [e]
  (when-not (nil? @phylocanvas-js-obj)
    (reset! groups (js->clj e))
    (set-phylocanvas-metadata!)
    ))

(defn group-select []
  (when-not (empty? @category-group-items)
    (multi-selectize
      "tree-groups"
      "Select groups to highlight"
      category-group-items
      groups
      group-change-handler!)))


(defn on-update-selected-nodes [node-ids]
  (reset! selected-nodes node-ids)
  (let [grouped-genomes (data/genome-groups-list->map (:genomes @data/tree-data))
        sel (flatten (map #(concat [%] (get grouped-genomes %)) @selected-nodes))
        ]
    (reset! selected-metadata (map #(get @data/genome-name-metadata %) sel))))

(defn phylocanvas-mounter! []
  (when-not (nil? @data/tree-data)
    (set! (.-innerHTML (utils/by-id "phylocanvas")) "")
    (let []
      (reset! phylocanvas-js-obj (new js/Phylocanvas.Tree
                               "phylocanvas"
                               (clj->js {"treeType" "circular"
                                         "showLabels" false})))
      (.load @phylocanvas-js-obj (:tree @data/tree-data))
      (aset js/window "pc" @phylocanvas-js-obj)
      (set-phylocanvas-metadata!)
      ;(phylocanvas-selection-change-watcher)
      (.on @phylocanvas-js-obj "updated" #(on-update-selected-nodes (aget % "nodeIds")))
      )
    ))

(defn phylocanvas-component []
  (reagent/create-class
    {:render               (fn []
                             (deref data/tree-data)
                             [:div#phylocanvas {:style {:border "#aaa solid 1px"
                                                 :height 600}}])
     :component-did-mount  phylocanvas-mounter!
     :component-did-update phylocanvas-mounter!
     }))


(defn tree-plot-phylocanvas
  "Render tree visualization when tree page has been visited"
  []
  (when @show-tree?
    [phylocanvas-component]
    ))

(defn get-group-genomes [category group]
  (let [gs (flatten (:genomes @data/tree-data))]
    (->> gs
         (map #(get @data/genome-name-metadata %))
         (filter (fn [xs]
                   (= group ((keyword category) xs))))
         (map :genome))))

(defn tree-legend []
  (when-not (empty? @groups)
    [:div.tree-legend
     (let [groups @groups
           groups-colours (map (fn [x y] [x y]) groups (utils/qual-colours (count groups)))
           idx-groups-colours (map-indexed (fn [i x] [i x]) groups-colours)
           ]
       [:svg {:height (-> groups count inc (* 20) (+ 10))}
        [:text {:x     22
                :y     20
                :style {:font-weight "bold"}}
         @category]
        [:g
         (for [[idx [group colour]] idx-groups-colours]
           ^{:key [idx group colour]}
           [:g {:on-click #(let [genomes (get-group-genomes @category group)]
                            (on-update-selected-nodes genomes)
                            (.selectNodes @phylocanvas-js-obj (clj->js genomes)))
                          :transform (str "translate(0," (-> idx inc (* 20) (+ 10)) ")")}
            [:text {:x     22
                    :y     9
                    :dy    ".35em"
                    :style {:text-anchor "start"}
                    }
             (str group)]
            [:rect {:x      0
                    :width  18
                    :height 18
                    :fill   colour}]])]])]))

(defn tree-table-header []
  (when-not (empty? @selected-nodes)
    (let [n-nodes (count @selected-nodes)
          n-genomes (count @selected-metadata)
          node-label (str n-nodes " " (utils/pluralize "node" n-nodes))
          genome-label (str n-genomes " " (utils/pluralize "genome" n-genomes))]
      [:h2 (str "Tree selection table for " genome-label " from " node-label)])))



(defn tree-table []
  (when-not (empty? @selected-nodes)
    [slick-grid-table
     "tree-selection-table"
     selected-metadata
     :genome
     (map utils/keyword->str @data/default-cols)
     (atom [])
     table-grid
     table-dataview]))


(defn download-tree-table []
  (when-not (empty? @selected-nodes)
    ;; get the selected genomes from the MST node selection
    (let [n-genomes (count @selected-metadata)
          ;; get all metadata headers
          header-kws (->> @selected-metadata
                          first
                          keys
                          sort)
          ;; get CSV headers ordered by selected columns with the rest of the sorted columns appended
          ordered-header-kws (distinct (concat @data/default-cols header-kws))
          csv-data (utils/map->csv-text @selected-metadata ordered-header-kws)
          url-obj (utils/text->blob->obj-url csv-data)
          ]
      [:a.btn.btn-default {:href     url-obj
                           :download (str "subset-" n-genomes
                                          (utils/pluralize "genome" n-genomes)
                                          "-"
                                          (count @selected-nodes) "table.csv")}
       [:i.fa.fa-download.fa-lg.pull-left]
       " Download Table (CSV)"
       [:br]
       (str n-genomes " selected " (utils/pluralize "genome" n-genomes))])))

(defn tree-downloads []
  (when-not (empty? @data/tree-data)
    [:div.btn-group
     [bootstrap/download-text-button "tree.newick" "Download Newick" (:tree @data/tree-data)]
     #_[bootstrap/download-svg-button "Download tree plot as SVG" "tree" data/svg-data]
     ]
    ))

(defn tree-loading-indicator []
  (when (empty? @data/tree-data)
    [:div.alert.alert-warning
     [:h5 [:i.fa.fa-spinner.fa-spin.fa-lg] " Loading phylogenetic tree..."]]))

(defn tree-help-info []
  [:div
   [:p [:strong "Zoom in and out "] "by scrolling your mousewheel"]
   [:p [:strong "Click "] "a node to show metadata for all genomes belonging to that node"]
   [:p [:strong "Pan "] "by clicking and dragging within the plot area"]
   [:p "Select Genomes in 'Browse Genomes' to show tree with only selected Genomes"]
   [:p "View previous trees by clicking on an item in the View History panel"]])

(defn view-history-item [genomes tree-data]
  [:tr {:style {:background-color (if (= tree-data @data/tree-data) "rgba(0,100,255,0.2)" "transparent")}}
   (let [sel-hist-item (ffirst (filter (fn [[_ v]] (= genomes (sort v))) @data/genome-selection-collection))]
     [:td {:on-click #(reset! data/tree-data tree-data)
           :style    {:cursor "pointer"}}
      (when-not (nil? sel-hist-item)
        [:strong (str "[" sel-hist-item "] - ")])
      (str (->> tree-data :genomes count) " node(s); "
           (->> tree-data :genomes flatten count) " genome(s)")])
   [:td.pull-right
    [:button.destroy {:type     "button"
                      :on-click #(swap! data/tree-data-history dissoc genomes)}
     [:i.fa.fa-close.text-danger.fa-lg]]]])


(defn view-history-table []
  [:table.table.table-condensed
   [:tbody
    (map-indexed
      (fn [i [genomes tree-data]]
        ^{:key i}
        [view-history-item genomes tree-data])
      @data/tree-data-history)]])


(defn btns-show-selected-genomes
  "Render controls for showing selected subset of genomes in tree
  visualization."
  []
  (let [sorted-genome-names (->> @data/genome-name-metadata keys sort)
        first-tree (get @data/tree-data-history sorted-genome-names)
        curr-tree-genomes (->> @data/tree-data :genomes flatten sort)
        subset? (not= first-tree @data/tree-data)
        sorted-genome-selection (->> @data/genome-selection sort)]
    [:div
     (when subset?
       [:div
        [:button.btn.btn-success
         {:on-click (fn [_]
                      (if (nil? first-tree)
                        (go
                          (let [old-tree-data @data/tree-data]
                            (reset! data/tree-data nil)
                            (let [auth-header (<! (data/authorization-headers-for-current-user))
                                  resp (<! (data/get-phylo->chan "newick"
                                                                 :genomes sorted-genome-names
                                                                 :headers auth-header))
                                  resp-error (:error resp)]
                              (if resp-error
                                (do
                                  (utils/log-http-error :tree resp-error
                                                        :http-method "POST"
                                                        :uri (str data/api-uri "newick")
                                                        :message "Error occurred while retrieving tree data. Reloading previous tree data.")
                                  (reset! data/tree-data old-tree-data))
                                (reset! data/tree-data resp))
                              (when-not (or (nil? @data/tree-data)
                                            (some #(= @data/tree-data %) @data/tree-data-history))
                                (swap! data/tree-data-history assoc sorted-genome-names @data/tree-data)))))
                        (reset! data/tree-data first-tree)))}
         "Show all"]])
     (when-not (or (empty? sorted-genome-selection)
                   (= sorted-genome-selection curr-tree-genomes))
       [:button.btn.btn-danger
        {:on-click (fn [_]
                     (let [existing-tree-data (get @data/tree-data-history sorted-genome-selection)]
                       (if (nil? existing-tree-data)
                         (do
                           (reset! data/tree-data nil)
                           (go
                             (let [auth-header (<! (data/authorization-headers-for-current-user))
                                   resp (<! (data/get-phylo->chan "newick"
                                                                  :genomes sorted-genome-selection
                                                                  :headers auth-header))
                                   err-resp (:error resp)]
                               (if err-resp
                                 (do
                                   (utils/log-http data/app-log :tree err-resp
                                                   :message "Could not get tree"
                                                   :http-method "POST"
                                                   :uri "/api/newick"))
                                 (do
                                   (utils/log-http data/app-log :tree err-resp
                                                   :message "Retrieved tree!"
                                                   :http-method "POST"
                                                   :uri "/api/newick")
                                   (reset! data/tree-data resp)
                                   (when-not (or (nil? @data/tree-data)
                                                 (some #(= @data/tree-data %) @data/tree-data-history))
                                     (swap! data/tree-data-history assoc sorted-genome-selection @data/tree-data)))))))
                         (do
                           (reset! data/tree-data existing-tree-data)))
                       ))}
        (str "Show only " (count sorted-genome-selection) " genomes")])]))

(defn view-history-panel []
  [well-collapsible "tree-view-history-panel"
   [:span [:i.fa.fa-tree] " View Manager"]
   false
   [:div
    [:p [:strong "View History"]]
    [view-history-table]
    [btns-show-selected-genomes]]])

(defn tree-page []
  [:div {:style {:padding-left 300}}
   [:div.sidebar
    [:div.well.well-sm
     [well-collapsible "tree-help-info"
      [:span [:i.fa.fa-info-circle] " Help"]
      true
      [tree-help-info]]
     [tree-loading-indicator]
     [:div
      [category-select]
      [group-select]
      ]
     [selection/genome-selection-manager "tree-selection-manager" #(prn "tree-selection-manager on-change event")]
     [view-history-panel]
     ]]
   [:div.container-fluid
    [:div.row
     [:div.col-md-12
      (when @show-tree?
        [:div
         [:div.row
          [:div.col-md-10
           [tree-plot-phylocanvas]]
          [:div.col-md-2
           [tree-legend]]]
         [tree-downloads]
         [tree-table-header]
         [download-tree-table]
         [tree-table]])]]]])
