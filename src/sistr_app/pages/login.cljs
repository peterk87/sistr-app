(ns ^:figwheel-load sistr-app.pages.login
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljs.core.async :refer [put! chan <! timeout]]
    [reagent.core :as reagent :refer [atom]]
    [clojure.string :refer [split join]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [ajax.core :refer [POST GET PUT]]
    [sistr-components.bootstrap :refer [progress-bar panel]]
    [sistr-components.alerts :as alerts]
    [sistr-components.slickgrid :as slick-grid :refer [slick-grid-table]]
    [sistr-components.selectize :as selectize :refer [single-selectize]]
    [sistr-components.table :refer [Table]]
    [sistr-utils.csv :refer [read-csv!]]
    [reagent-forms.core :refer [bind-fields]]))

(def keycode->ascii
  {
   110 46
   109 45
   107 45
   173 45
   186 59
   187 61
   188 44
   189 45
   198 45
   190 46
   191 47
   192 96
   219 91
   220 92
   221 93
   222 39})

(def shift-keycode->char
  {
   96 "~"
   49 "!"
   50 "@"
   51 "#"
   52 "$"
   53 "%"
   54 "^"
   55 "&"
   56 "*"
   57 "("
   48 ")"
   45 "_"
   61 "+"
   91 "{"
   93 "}"
   92 "|"
   59 ":"
   39 "\""
   44 "<"
   46 ">"
   47 "?"
   })

(def token-uri (str data/api-uri "token"))
(def password-reset-uri (str data/api-uri "password_reset"))

(defn length-check
  [s & {:keys [max-len min-len] :or {min-len 4 max-len 20}}]
  (let [s-len (count s)]
    (and (>= s-len min-len)
         (<= s-len max-len))))

(defn regex-check
  [s & {:keys [re]
        ;:or {re #"^\w+$"}
        }]
  (boolean (re-matches re s))
  ;(boolean (re-matches re s))
  )

(defn email-check
  [s & {:keys [re]
        ;:or {re #"^\S+@\S+\.\S+$"}
        }]
  (regex-check s :re #"^\S+@\S+\.\S+$")
  ;(regex-check s :re re)
  )

(defn word-char?
  "Check if character is word [0-9a-zA-Z_]|\\w"
  [c]
  (->> c
       (re-matches #"^\w$")
       nil?
       not))

(defonce user-registration-info (atom {:username        nil
                                       :user-exists?    nil
                                       :password        nil
                                       :password-valid? false
                                       :email           nil
                                       :email-valid?    nil}))

(defonce user-info (atom nil))



(defn get-user-info [user token out]
  (go
    (let [auth-headers (utils/auth-headers token)
          uri (utils/get-base-uri data/api-uri user)
          resp (<! (utils/get-uri->chan uri :headers auth-headers))
          err-resp (:error resp)]
      (if err-resp
        (do
          (utils/log-http data/app-log :user-info err-resp
                          :message (str "Unable to retrieve user info for '" user "'")
                          :uri uri)
          (utils/log-http-error data/http-errors :user-info err-resp
                                :message (str "Unable to retrieve user info for '" user "'")
                                :uri uri))
        (do
          (utils/log-http data/app-log :user-info resp
                          :message (str "Retrieved user info for '" user "'")
                          :uri uri)
          (reset! out resp))))))

(add-watch data/sistr-user-token :get-user-info
           #(do
             (js/console.info "Getting user info for " @data/sistr-user)
             (get-user-info @data/sistr-user %4 user-info)))

(defn restrict-input-on-key-down!
  "Restrict user input on keydown event to alphanumeric and underscore characters.
  With email addresses allow '@', '.', ',', '-' as well"
  [e &
   {:keys [on-enter! email?]
    :or   {on-enter! (fn [] (prn "enter!"))
           email?    false}}]
  (let [keycode (.-keyCode e)
        ascii-keycode (get keycode->ascii keycode)
        shift? (.-shiftKey e)
        shift-char (get shift-keycode->char (if ascii-keycode ascii-keycode keycode))
        num-keycodes (into #{} (range 48 58))
        ascii-keycodes (into #{} (keys keycode->ascii))]
    (js/console.info "ascii" ascii-keycode
                     "keycode" keycode
                     "shift" shift?
                     "char" (js/String.fromCharCode keycode) (js/String.fromCharCode ascii-keycode)
                     "is-word?" (word-char? (js/String.fromCharCode keycode)))
    (condp #(some %1 [%2]) keycode
      ; Enter key
      #{13} (do
              (.preventDefault e)
              (on-enter!))
      ; numpad special keys
      #{111 110 107 106} (.preventDefault e)
      ; allow -/. for email only
      #{44 46 45 189 109} (when (not email?)
                            (do
                              (.preventDefault e)))
      ; allow '.', but not '>' (shift + '.')
      #{190} (when (and shift? email?)
               (.preventDefault e))
      ; allow @ for email only
      #{50} (when (and shift? (not email?))
              (do
                (.preventDefault e)))
      ; symbols on shift+0-9
      num-keycodes (when shift? (.preventDefault e))
      ; special symbols and conversions
      ascii-keycodes (if shift?
                       (do
                         (when-not (word-char? shift-char)
                           (.preventDefault e)))
                       (do
                         (when-not (word-char? (js/String.fromCharCode ascii-keycode))
                           (.preventDefault e))))
      ; default case, don't preventDefault event so allow keydown to register
      (do
        (js/console.info "keydown" keycode (.-target.value e))
        ))))

(defn check-user-exists
  "Check API if user exists. Return result by atom with true if user exists, false if HTTP code 404 (not found),
   and nil if other error code."
  [username atom-user-exists? & {:keys [headers] :or {headers {}}}]
  (go (let [uri (str data/api-uri "check_user/" username)
            resp (<! (utils/get-uri->chan uri :headers headers))
            error-resp (:error resp)]
        (js/console.info (clj->js resp))
        (if error-resp
          (cond
            (and (= (:status error-resp) 404)
                 (empty? (:response error-resp))) (reset! atom-user-exists? false)
            :else (reset! atom-user-exists? nil)
            )
          (reset! atom-user-exists? true)))))

(defn login! [user password]
  (go (let [uri token-uri
            resp (<! (utils/get-auth-token uri user password))
            resp-error (:error resp)
            err-msg "Error occurred trying to login. Authentication credentials invalid."]
        (if resp-error
          (do
            (js/console.info "token retrieval error"
                             (clj->js (:status resp-error))
                             (clj->js (get-in resp-error [:response :error])))
            (utils/log-http-error data/http-errors :login resp-error
                                  :message err-msg
                                  :uri uri)
            (utils/log-http data/app-log :login-error resp-error
                            :message err-msg
                            :uri uri)
            (reset! user-info nil)
            (reset! data/sistr-user-token nil))
          (do
            (js/console.info "Retrieved token" (clj->js resp))
            (swap! data/http-errors dissoc :login)
            (utils/log-http data/app-log :login resp
                            :message (str "Log in successful as '" user "'!")
                            :uri uri)
            (reset! data/sistr-user user)
            (reset! data/sistr-user-token resp)
            (get-user-info @data/sistr-user @data/sistr-user-token user-info)
            (data/get-app-data!))))))

(defn login-form []
  (let [username (atom (if (= @data/sistr-user data/public-sistr-user)
                         ""
                         @data/sistr-user))
        password (atom "")]
    (fn []
      [:div.panel.panel-default
       [:div.panel-heading
        [:h2 "Login"]]
       [:div.panel-body
        [:div.form
         [:div.row
          [:div.col-md-4
           [:label "Username"]]
          [:div.col-md-8
           [:input.form-control {:type      "text"
                                 :value     @username
                                 :on-change #(reset! username (.-target.value %))}
            ]]]
         [:div.row
          [:div.col-md-4
           [:label "Password"]]
          [:div.col-md-8
           [:input.form-control {:type      "password"
                                 :value     @password
                                 :on-change #(reset! password (.-target.value %))}]]]
         ; TODO: reagent cursor for login errors?
         (let [login-error (:login @data/http-errors)]
           (when login-error
             (let [{:keys [status http-method uri message error timestamp]} login-error]
               [:div.alert.alert-danger
                [:h4 "Username/password credentials incorrect!"]
                [:p [:strong "Message: "] message]
                [:p [:strong "HTTP code "] [:span.badge (str status)]
                 " on " [:strong http-method] " " [:em uri]]
                [:p [:strong "Error: "] error]
                [:p [:em (str timestamp)]]])))
         [:div.row
          [:div.col-md-4
           (let [args (if (or (empty? @username) (empty? @password))
                        {:disabled "disabled"}
                        {})]
             [:button.btn.btn-primary
              (merge args
                     {:on-click
                      #(login! @username @password)})
              "Login"])]]]]])))

(defn email-input
  [email email-valid? &
   {:keys [label placeholder on-enter! on-blur!]
    :or   {label       "Enter an email address (optional)"
           placeholder "Email for password recovery"
           on-enter!   nil
           on-blur!    nil}}]
  [:div.row
   [:div.col-md-2
    [:label.control-label label]]
   [:div.col-md-6
    [:div.input-group
     [:span.input-group-addon [:i.fa.fa-envelope]]
     [:input.form-control {:type        "email"
                           :placeholder placeholder
                           :value       @email
                           :on-key-down #(restrict-input-on-key-down! %
                                                                      :email? true
                                                                      :on-enter! on-enter!)
                           :on-change   #(do
                                          (reset! email (.-target.value %)))
                           :on-blur     (if (nil? on-blur!)
                                          on-enter!
                                          on-blur!)
                           }]
     ]]
   (when (and (not (empty? @email))
              (not @email-valid?))
     [:div.col-md-4
      [:div.alert.alert-danger "Invalid email!"]])
   ])

(defn username-input [username & {:keys [on-change! on-enter! on-blur! components]
                                  :or   {
                                         on-change! nil
                                         on-enter!  nil
                                         on-blur!   nil
                                         components nil}}]
  [:div.row
   [:div.col-md-2
    [:label.control-label "Username"]]
   [:div.col-md-6
    [:div.input-group
     [:span.input-group-addon [:i.fa.fa-user]]
     [:input.form-control {:type        "text"
                           :placeholder "Enter your username"
                           :value       @username
                           :on-key-down #(restrict-input-on-key-down! % :on-enter! on-enter!)
                           :on-change   (if (nil? on-change!)
                                          #(reset! username (.-target.value %))
                                          on-change!)
                           :on-blur     on-blur!}]]]])

(defn username-registration-input [username atom-user-exists? &
                                   {:keys [label]
                                    :or   {label "Pick a username"}}]
  (letfn [(valid-username? [username]
            (if (empty? username)
              false
              (not (->> (re-matches #"^\w+$" username) (nil?)))))
          (on-enter! []
            (when (and
                    (nil? @atom-user-exists?)
                    (valid-username? @username))
              (check-user-exists @username atom-user-exists?)))]
    [:div.row
     [:div.col-md-2
      [:label.control-label label]]
     [:div.col-md-6
      [:div.input-group {:class (cond
                                  (nil? @atom-user-exists?) ""
                                  (= @atom-user-exists? true) "has-error has-feedback"
                                  (= @atom-user-exists? false) "has-success has-feedback")}
       [:span.input-group-addon [:i.fa.fa-user]]
       [:input.form-control {:type        "text"
                             :placeholder "Username"
                             :value       @username
                             :on-key-down #(restrict-input-on-key-down! % :on-enter! on-enter!)
                             :on-change   #(do
                                            (js/console.log "user name reg input on-change" (.-target.value %))
                                            (reset! atom-user-exists? nil)
                                            (reset! username (.-target.value %)))
                             :on-blur     #(when (and
                                                   (nil? @atom-user-exists?)
                                                   (valid-username? @username))
                                            (check-user-exists @username atom-user-exists?))
                             }]]]
     (when (and (not (valid-username? @username))
                (not (empty? @username)))
       [:div.col-md-4
        [:div.alert.alert-danger "Invalid username! Numbers, letters and underscores only!"]])
     #_(prn "atom user exists" atom-user-exists?)
     (when-not (or (not (valid-username? @username)) (nil? @atom-user-exists?))
       [:div.col-md-4
        (when @atom-user-exists?
          [:div.alert.alert-danger "Username " [:strong @username] " already taken!"])])
     ]))

(defn user-registration-password [password password-valid?]
  (let [password-repeat (atom nil)]
    (add-watch password-repeat :valid?
               #(do
                 (prn "password-repeat valid?" %4)
                 (cond
                   (not= @password %4) (reset! password-valid? false)
                   (< (count @password) 6) (reset! password-valid? false)
                   (empty? @password) (reset! password-valid? false)
                   (empty? %4) (reset! password-valid? false)
                   :else (reset! password-valid? true)
                   )))
    (fn []
      [:div
       [:div.row
        [:div.col-md-2
         [:label.control-label "Password"]]
        [:div.col-md-6
         [:div.input-group {:class (when-not (and (empty? @password)
                                                  (empty? @password-repeat))
                                     (if @password-valid?
                                       "has-success has-feedback"
                                       "has-error has-feedback"))}
          [:span.input-group-addon [:i.fa.fa-lock]]
          [:input.form-control {:type        "password"
                                :placeholder "Password"
                                :value       @password
                                :on-key-down #(restrict-input-on-key-down! %)
                                :on-change   #(do (reset! password (-> % .-target .-value)))}]
          ]
         ]
        [:div.col-md-4 (cond
                         (and (empty? @password) (empty? @password-repeat)) nil
                         (not= @password @password-repeat) [:div.alert.alert-danger "Passwords do not match!"]
                         (< (count @password) 6) [:div.alert.alert-danger "Password must be more than 6 characters long!"]
                         (empty? @password) [:div.alert.alert-danger "Password with >6 characters required!"]
                         (empty? @password-repeat) [:div.alert.alert-danger "You need to re-enter your password!"]
                         :else [:div.alert.alert-success "Passwords match! :)"]
                         )]]
       [:div.row
        [:div.col-md-2
         [:label.control-label "Re-enter Password"]]
        [:div.col-md-6
         [:div.input-group {:class (when-not (and (empty? @password)
                                                  (empty? @password-repeat))
                                     (if @password-valid?
                                       "has-success has-feedback"
                                       "has-error has-feedback"))}
          [:span.input-group-addon [:i.fa.fa-lock]]
          [:input.form-control {:type        "password"
                                :placeholder "Re-enter Password"
                                :value       @password-repeat
                                :on-key-down #(restrict-input-on-key-down! %)
                                :on-change   #(do (reset! password-repeat (-> % .-target .-value)))}]]]]])))

(defn create-new-user!
  [username password & {:keys [email] :or {email nil}}]
  (go (let [resp (<! (utils/create-registered-user data/api-uri username password
                                                   :email email))
            err-resp (:error resp)]
        #_(prn "create new user " resp)
        (if err-resp
          (do
            (utils/log-http data/app-log :create-user err-resp
                            :message "Error creating new user"
                            :uri (str data/api-uri username)
                            :http-method "POST")
            )
          (do
            (utils/log-http data/app-log :create-user resp
                            :message (str "Created new registered user '" username "'")
                            :uri (str data/api-uri username)
                            :http-method "POST")
            (reset! user-registration-info {:username        nil
                                            :user-exists?    nil
                                            :password        nil
                                            :password-valid? false
                                            :email           nil
                                            :email-valid?    nil})
            (reset! data/sistr-user (:name resp))
            (reset! data/sistr-user-token (:token resp))
            (get-user-info @data/sistr-user @data/sistr-user-token user-info)
            (data/get-app-data!))))))

(defn registration-form []
  (let [username (reagent/cursor user-registration-info [:username])
        user-exists? (reagent/cursor user-registration-info [:user-exists?])
        password (reagent/cursor user-registration-info [:password])
        password-valid? (reagent/cursor user-registration-info [:password-valid?])
        email (reagent/cursor user-registration-info [:email])
        email-valid? (reagent/cursor user-registration-info [:email-valid?])]
    (fn []
      [:div.panel.panel-default
       [:div.panel-heading
        [:h2 [:i.fa.fa-user-plus] " Create New User"]]
       [:div.panel-body
        [:div.container-fluid
         [:div.row
          [:div.col-md-6
           [username-registration-input username user-exists?]
           [user-registration-password password password-valid?]
           [email-input email email-valid? :on-enter! (fn [] (reset! email-valid? (email-check @email)))]
           ]
          [:div.col-md-6
           [:p "Enter a username and password to create a user account."]
           [:p "You can provide a valid email address to recover your username/password in case you forget your login credentials."]
           [:em.text-danger "Username must be untaken!"]

           (when (and (or @user-exists? (empty? @username)) (not (nil? @username)))
             [:div.alert.alert-danger "Choose a different username!"])
           (when-not @password-valid?
             [:div.alert.alert-danger "Password not valid"])

           (when-not (or (nil? @email-valid?)
                         @email-valid?
                         (empty? @email))
             [:div.alert.alert-danger "Email invalid! <user>@<domain>.<com/org/etc> expected"])]]
         [:div.row
          [:button.btn.btn-primary
           {:class    (if (or (and (not @email-valid?) (not (empty? @email)))
                              (not @password-valid?)
                              @user-exists?)
                        "disabled"
                        "")
            :on-click #(create-new-user! @username @password
                                         :email @email)}
           [:i.fa.fa-user-plus] " Create new user"]]
         ]]])))

(defn replace-values-list-maps [data k old-value new-value]
  (map #(if (= (k %) old-value)
         (assoc % k new-value)
         %)
       data))

(defn update-user-info! [data]
  (let [username @data/sistr-user
        token @data/sistr-user-token
        auth-headers (if token (utils/auth-headers token)
                               ; else if token nil then generate auth headers from the username only
                               (utils/auth-headers username "x"))
        uri (str data/api-uri "user/" username)]
    (go (let [resp (<! (utils/put->chan
                         uri
                         :data data
                         :headers auth-headers))
              err-resp (:error resp)]
          (if err-resp
            (do
              (js/console.warn "PUT user error" (clj->js err-resp))
              (utils/log-http data/app-log :update-user err-resp
                              :message (str "Could not update user info for '" username "'")
                              :uri uri
                              :http-method "PUT"))
            (do
              (utils/log-http data/app-log :update-user resp
                              :message (str "Successfully updated user info")
                              :uri uri
                              :http-method "PUT")
              (let [old-user-name username
                    new-user-name (:name resp)
                    new-token (:token resp)]
                (js/console.info "PUT user success" old-user-name new-user-name (clj->js resp))
                (when (not= new-user-name old-user-name)
                  (reset! data/sistr-user new-user-name)
                  ; update user in metadata and serovar results lists of maps
                  (let [new-md (replace-values-list-maps
                                 (vals @data/genome-name-metadata)
                                 :user_uploader
                                 old-user-name
                                 new-user-name)
                        genome-names (map :genome new-md)]
                    (reset! data/genome-name-metadata (zipmap genome-names new-md)))
                  (let [new-md (replace-values-list-maps
                                 @data/serovar-prediction-results
                                 :user_uploader
                                 old-user-name
                                 new-user-name)]
                    (reset! data/serovar-prediction-results new-md))
                  )
                (reset! data/sistr-user-token new-token)
                ; user info should update automatically
                )))))))

(defn validate-user-update-data [data]
  (let [{:keys [new_username password email]} data]
    (cond
      (and new_username (nil? (re-matches #"\w+" new_username))) false
      (and password (< (count password) 6)) false
      (and email (not (email-check email))) false
      :else true)))

(defn dissoc-when [m pred]
  (apply dissoc m (filter #(pred (m %)) (keys m))))

(def user-updating? (atom false))

(defn acct-mngt-form []
  (let [new-username (atom "")
        user-exists? (atom nil)
        password (atom "")
        password-valid? (atom nil)
        email (reagent/cursor user-info [:email])
        email-valid? (atom (if (nil? @email) nil (email-check @email)))]
    (fn []
      [:div.panel.panel-default
       [:div.panel-heading
        [:h2 [:i.fa.fa-user-secret] " User Account Management"]]
       [:div.panel-body
        [:div.container-fluid
         [:div.row
          [:div.col-md-6
           [username-registration-input new-username user-exists?
            :label "New username"]
           [user-registration-password password password-valid?]
           [email-input email email-valid?
            :on-enter! (fn []
                         (reset! email-valid? (if (nil? @email)
                                                nil
                                                (email-check @email))))]
           ]
          [:div.col-md-6
           [:p "Change your username and/or password."]
           [:p "You can provide a valid email address to recover your username/password in case you forget your login credentials."]
           (when (and @user-exists? (not (empty? @new-username)))
             [:div.alert.alert-danger "Choose a different username!"])
           (when-not (or @password-valid? (empty? @password))
             [:div.alert.alert-danger "Password not valid"])

           (when-not (or (nil? @email-valid?)
                         @email-valid?
                         (empty? @email))
             [:div.alert.alert-danger "Email invalid! <user>@<domain>.<com/org/etc> expected"])]]
         (when @user-updating?
           [:div.row
            [:div.col-md-6
             [:div.alert.alert-info
              [:h4 [:i.fa.fa-spinner.fa-spin.fa-lg] " Updating user info"]]]])
         [:div.row
          [:button.btn.btn-primary
           {:class    (if (or (and (not @email-valid?) (not (empty? @email)))
                              (and (not @password-valid?) (not (empty? @password)))
                              (and (not (empty? @new-username)) @user-exists?))
                        "disabled"
                        "")
            :on-click #(let [put-data (dissoc-when {:new_username @new-username
                                                    :password     @password
                                                    :email        @email}
                                                   empty?)]
                        (reset! user-updating? true)
                        (when (validate-user-update-data put-data)
                          (update-user-info! put-data))
                        (reset! user-updating? false)
                        )}
           [:i.fa.fa-user-plus] " Update user info"]]
         ]]])))

(defn user-acct-page []
  [:div {:style {:padding-left 300}}
   [:div.sidebar
    [:div.well.well-sm
     [:h4 "Welcome back, " [:strong @data/sistr-user] "! " [:i.fa.fa-smile-o]]
     [:p "User role: " [:strong (:role @user-info)]]
     (let [n-genomes (count (:genomes @user-info))]
       [:p "You have uploaded " [:strong n-genomes] " " (utils/pluralize "genome" n-genomes) " to SISTR!"])
     [:p "Manage your user account information on this page."]
     ]
    ]
   [:div.container-fluid
    [:div.row
     [:div.col-md-10
      [acct-mngt-form]
      #_[Table
         data/genome-name-metadata
         data/categories]
      ]]]])

(defonce pw-resetting? (atom false))
(defonce pw-reset-resp (atom nil))

(defn request-password-reset! [username email]
  (go (let [_ (reset! pw-reset-resp nil)
            _ (reset! pw-resetting? true)
            uri password-reset-uri
            resp (<! (utils/get-uri->chan uri :params {:username username
                                                       :email    email}))]
        (reset! pw-reset-resp resp)
        (reset! pw-resetting? false)
        )))

(defn password-reset-form []
  (let [email (atom "")
        email-valid? (atom nil)
        username (atom "")]
    (fn []
      [:div.form-horizontal
       [:h3 "Forgot your password?"]
       [:p "Enter your username or email address to have your password reset and a temporary password sent to you by email!"]
       [username-input username]
       [:h4 "OR"]
       [email-input email email-valid?
        :label "Email"
        :placeholder "User account email"
        :on-enter! (fn [] (reset! email-valid? (email-check @email)))]
       (when-not (nil? @pw-reset-resp)
         (let [pw-reset-error (:error @pw-reset-resp)]
           (if pw-reset-error
             (let [{:keys [status http-method uri message error timestamp]} pw-reset-error]
               [:div.alert.alert-danger
                [:h4 "Could not process password reset request!"]
                [:p [:strong "Message: "] message]
                [:p [:strong "HTTP code "] [:span.badge (str status)]
                 " on " [:strong http-method] " " [:em uri]]
                [:p [:strong "Error: "] error]
                [:p [:em (str timestamp)]]])
             [:div.alert.alert-success
              [:h4 "Password reset request received!"]
              [:p "Please check your email for your new login credentials."]])))
       [:button.btn.btn-primary {:class    (cond
                                             (and (empty? @email) (empty? @username)) "disabled"
                                             (and (empty? @username) (not @email-valid?)) "disabled"
                                             @pw-resetting? "disabled"
                                             :else ""
                                             )
                                 :on-click #(request-password-reset! @username @email)}
        "Request password reset"]
       ]))
  )

(defn login-page []
  [:div {:style {:padding-left 300}}
   [:div.sidebar
    [:div.well.well-sm
     [:h4 "Login/register a new user account here!"]
     [:p "Entering your email is optional and it will only ever be used for password recovery."]
     [:div.alert.alert-warning
      [:h4 [:i.fa.fa-exclamation-triangle] " Warning!"]
      [:p "You can still upload genomes without creating a registered user account"]
      [:em "Note: temporary user accounts and genomes only persist for 2 weeks after last accessed!"]]
     ]
    ]
   [:div.container-fluid
    [:ul.nav.nav-tabs {:role "tablist"}
     [:li {:role  "presentation"
           :class "active"
           }
      [:a {:href        "#login-form"
           :role        "tab"
           :data-toggle "tab"}
       "Login"]]
     [:li {:role "presentation"
           ;:class "active"
           }
      [:a {:href        "#create-user-form"
           :role        "tab"
           :data-toggle "tab"}
       [:i.fa.fa-user-plus] " Create new user"]]
     [:li {:role "presentation"
           ;:class "active"
           }
      [:a {:href        "#forgot-password-form"
           :role        "tab"
           :data-toggle "tab"}
       "Forgot password?"]]]
    ; Tabs for logging in, creating new user and resetting password
    [:div.tab-content
     [:div.tab-pane {:role "tabpanel"
                     :id   "login-form"
                     :class "active"
                     }
      [:div.row
       [:div.col-md-4
        [login-form]]
       [:div.col-md-4
        [:div.alert.alert-info
         [:p "Please enter your login credentials"]]]]]
     [:div.tab-pane {:role "tabpanel"
                     :id   "create-user-form"
                     ;:class "active"
                     }
      [registration-form]]
     [:div.tab-pane {:role "tabpanel"
                     :id   "forgot-password-form"
                     ;:class "active"
                     }
      [password-reset-form]]
     ]]])

(defn user-page []
  (if (or (not= @data/sistr-user data/public-sistr-user)
          (and (= @data/sistr-user data/public-sistr-user) @data/sistr-user-token))
    [user-acct-page]
    [login-page]))
