(ns ^:figwheel-load sistr-app.pages.about
  (:require
    [reagent.core :refer [atom]]
    [sistr-data.data :as data]
    [sistr-components.bootstrap :refer [panel]]))

(defn app-data-status [d label]
  (let [ready? (not (or (nil? @d)
                        (empty? @d)))]
    [:div.alert
     {:class (str "alert-" (if ready? "success"
                                      "danger"))}
     [:i.fa {:class (str "fa-" (if ready? "check"
                                          "spinner fa-spin"))}]
     (str " "
          label
          (if ready? " - Loaded!"
                     " - Loading..."))]))

(defn web-browser-warning []
  [:div.alert.alert-warning
   [:i.fa.fa-exclamation-triangle.fa-2x.pull-left]
   [:p "For the best experience using SISTR, a recent version of "]
   [:ul
    [:li
     [:a {:href   "https://www.google.com/chrome/browser/"
          :target "_blank"}
      [:i.fa.fa-chrome.fa-lg] " Google Chrome"] " or"]
    [:li
     [:a {:href   "https://www.mozilla.org/firefox"
          :target "_blank"}
      [:i.fa.fa-firefox.fa-lg] " Firefox"]]]
   [:p " is recommended."]
   [:p
    [:span.fa-stack
     [:i.fa.fa-internet-explorer.text-info.fa-stack-2x]
     [:i.fa.fa-ban.text-danger.fa-stack-2x]]
    " Internet Explorer is " [:strong "unsupported"]
    ]])

(defn sistr-news []
  [panel "sistr-news-panel"
   [:h4 [:i.fa.fa-bullhorn] " SISTR News "]
   "warning"
   false
   [:table.table.table-condensed.table-striped
    [:tbody
     [:tr.success
      [:td [:i.fa.fa-envelope]]
      [:td
       [:p "Comments, issues, contributions?"]
       [:p "Help us make SISTR better!"]
       [:p "Contact us at "
        [:a {:href   "mailto:sistr.salmonella@gmail.com"
             :target "_blank"}
         "sistr.salmonella@gmail.com"]]]
      [:td]]
     [:tr
      [:td [:i.fa.fa-gears]]
      [:td
       [:span [:a {:href   "https://github.com/peterk87/sistr_cmd/releases/tag/v0.3.1"
                   :target "_blank"}
               " sistr_cmd v0.3.1 released! "]
        "Install with "
        [:code "pip install sistr_cmd"]
        " for offline SISTR analyses! "
        [:a {:href "https://github.com/peterk87/sistr_cmd/"
             :target "_blank"}
         "More info here"]]]
      [:td [:span.label.label-default "Feb 14, 2017"]]
      ]
     [:tr
      [:td [:i.fa.fa-cubes]]
      [:td
       [:span [:a {:href   "https://github.com/peterk87/sistr-packer"
                   :target "_blank"}
               " Update for SISTR Virtual Appliance build scripts (sistr_backend v1.1; DB v1.0)"]]]
      [:td [:span.label.label-default "June 23, 2016"]]
      ]
     [:tr
      [:td [:i.fa.fa-database]]
      [:td
       [:span [:a {:href   "https://lfz.corefacility.ca/sistr-db-dumps/sistr_db-7511_public-2016_06_17-no_contigs.sql.gz"
                   :target "_blank"}
               " New SISTR DB dump available (7511 genomes) (2.4GB)"]]]
      [:td [:span.label.label-default "June 23, 2016"]]
      ]
     [:tr
      [:td [:i.fa.fa-rocket]]
      [:td
       [:span " Added "
        [:a {:href "http://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0997-x" :target "_blank"}
         "Mash MinHash"]
        " search against "
        [:a {:href "https://mash.readthedocs.io/en/latest/tutorials.html#querying-read-sets-against-an-existing-refseq-sketch" :target "_blank"}
         "55k NCBI RefSeq genomes"]
        ]]
      [:td [:span.label.label-default "May 17, 2016"]]
      ]]]])

(defn development-exclaimer []
  [:div.alert.alert-danger
   [:i.fa.fa-wrench.fa-3x.pull-left]
   [:p [:strong "This app is in highly active development!"]]
   [:p [:em "Please report bugs and issues "
        [:a {:href "https://bitbucket.org/peterk87/sistr_backend/issues?status=new&amp;status=open"
             :target "_blank"} "here"]]]])

(defn publications-of-interest []
  [panel
   "pubs-of-interest"
   [:h4 [:i.fa.fa-book] " Publications of Interest"]
   "default"
   false
   [:ul
    [:li [:a {:href "http://genomebiology.biomedcentral.com/articles/10.1186/s13059-016-0997-x" :target "_blank"}
          [:strong "Mash: fast genome and metagenome distance estimation using MinHash. "]
          "Brian D. Ondov, Todd J. Treangen, Páll Melsted, Adam B. Mallonee, Nicholas H. Bergman, Sergey Koren and Adam M. Phillippy. "
          [:em "Genome Biology 2016 17:132. DOI: 10.1186/s13059-016-0997-x"]]]
    [:li [:a {:href "https://bitbucket.org/peterk87/microbialinsilicotyper/wiki/mist_paper.pdf" :target "_blank"}
          [:strong "[PDF] "]
          [:strong "MIST: a tool for rapid in silico generation of molecular data from bacterial genome sequences. "]
          "Kruczkiewicz, Peter and Mutschall, Steven and Barker, Dillon and Thomas, James and Van Domselaar, Gary and Gannon, Victor P.J. and Carrillo, Catherine D. and Taboada, Eduardo N. "
          [:em "Proceedings of Bioinformatics 2013: 4th International Conference on Bioinformatics Models, Methods and Algorithms. 2013. 316–323."]]]
    [:li [:a {:href "http://www.ncbi.nlm.nih.gov/pubmed/25219780" :target "_blank"}
          [:strong "Multi-laboratory evaluation of the rapid genoserotyping array (SGSA) for the identification of " [:em "Salmonella"] " serovars. "]
          "Yoshida C, Lingohr EJ, Trognitz F, MacLaren N, Rosano A, Murphy SA, Villegas A, Polt M, Franklin K, Kostic T; OIE Reference Laboratory for Salmonellosis; Austrian Agency for Health and Food Safety, Kropinski AM, Card RM. "
          [:em "Diagn Microbiol Infect Dis. 2014 Aug 29. pii: S0732-8893(14)00324-1. doi: 10.1016/j.diagmicrobio.2014.08.006."]]]
    [:li [:a {:href "http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3147765/" :target "_blank"}
          [:strong "Rapid Genoserotyping Tool for Classification of " [:em "Salmonella"] " Serovars. "]
          "Kristyn Franklin, Erika J. Lingohr, Catherine Yoshida, Muna Anjum, Levente Bodrossy, Clifford G. Clark, Andrew M. Kropinski, and Mohamed A. Karmali. "
          [:em "J Clin Microbiol. 2011 August; 49(8): 2954–2965."]]]
    [:li [:a {:href "https://www.sciencedirect.com/science/article/pii/S0167701207001662" :target "_blank"}
          [:strong "Methodologies towards the development of an oligonucleotide microarray for determination of " [:em "Salmonella"] " serotypes. "]
          "Catherine Yoshida, Kristyn Franklin, Paulina Konczy, John R. McQuiston, Patricia I. Fields, John H. Nash, Ed N. Taboada, Kris Rahn. "
          [:em "Journal of Microbiological Methods. Volume 70, Issue 2, August 2007, Pages 261–271"]]]
    [:li [:a {:href "http://www.annualreviews.org/doi/abs/10.1146/annurev.micro.59.030804.121325" :target "_blank"}
          [:strong "Multilocus sequence typing of bacteria. "]
          "Martin C.J. Maiden. "
          [:em "Annu Rev Microbiol. 2006 June; 60: 561–588."]]]
    ]])

(defn serovar-variant-warning-info []
  [:div
   [:p "SISTR does not identify or report serovar variants requiring biochemical or sub-speciation tests for full characterization. SISTR infers a serovar which may require additional phenotypic information for final characterization in the following circumstances:"]
   [:ol
    [:li "When multiple serovars have the same antigenic formula in the White-Kauffmann-Le Minor scheme."]
    [:li "When a partial antigenic formula is identified."]
    [:li "For exceptions (eg : Choleraesuis/Paratyphi C/Typhisuis/Chiredzi, Sendai/Miami, Paratyphi B/Paratyphi B var. Java)"]]])

(defn serovar-variant-warning-panel [& {:keys [id] :or {id "serovar-variant-warning"}}]
  [panel
   id
   [:h4 [:i.fa.fa-exclamation-triangle] " Serovar prediction of serovar variants"]
   "warning"
   false
   [serovar-variant-warning-info]])

(defn main-spiel []
  [:div
   [:p
    [:b "Citation: "]
    [:a {:href "http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101" :target "_blank"}
     "The " [:em "Salmonella In Silico"] " Typing Resource (SISTR): an open web-accessible tool for rapidly typing and subtyping draft " [:em "Salmonella"] " genome assemblies. "
     "Catherine Yoshida, Peter Kruczkiewicz, Chad R. Laing, Erika J. Lingohr, Victor P.J. Gannon, John H.E. Nash, Eduardo N. Taboada. "
     [:em "PLoS ONE 11(1): e0147101. doi: 10.1371/journal.pone.0147101"]]
    ]
   [:p "For nearly 100 years serotyping has been the gold standard for the identification of Salmonella serovars. Despite the increasing adoption of DNA-based subtyping approaches, serotype information remains a cornerstone in food safety and public health activities aimed at reducing the burden of salmonellosis. At the same time, recent advances in whole-genome sequencing (WGS) promise to revolutionize our ability to perform advanced pathogen characterization in support of improved source attribution and outbreak analysis. We present the Salmonella In Silico Typing Resource (SISTR), a bioinformatics platform for rapidly performing simultaneous in silico analyses for several leading subtyping methods on draft Salmonella genome assemblies. In addition to performing serovar prediction by genoserotyping, this resource integrates sequence-based typing analyses for: Multi-Locus Sequence Typing (MLST), ribosomal MLST (rMLST), and core genome MLST (cgMLST). We show how phylogenetic context from cgMLST analysis can supplement the genoserotyping analysis and increase the accuracy of in silico serovar prediction to over 94.6% on a dataset comprised of 4,188 finished genomes and WGS draft assemblies. In addition to allowing analysis of user-uploaded whole-genome assemblies, the SISTR platform incorporates a database comprising over 4,000 publicly available genomes, allowing users to place their isolates in a broader phylogenetic and epidemiological context. The resource incorporates several metadata driven visualizations to examine the phylogenetic, geospatial and temporal distribution of genome-sequenced isolates. As sequencing of Salmonella isolates at public health laboratories around the world becomes increasingly common, rapid in silico analysis of minimally processed draft genome assemblies provides a powerful approach for molecular epidemiology in support of public health investigations. Moreover, this type of integrated analysis using multiple sequence-based methods of sub-typing allows for continuity with historical serotyping data as we transition towards the increasing adoption of genomic analyses in epidemiology."]])

(defn links-of-interest []
  [panel
   "pubs-of-interest"
   [:h4 [:i.fa.fa-external-link] " Links"]
   "default"
   false
   [:ul
    [:li [:a {:href "https://lfz.corefacility.ca/sistr-backend-docs/" :target "_blank"}
          "SISTR documentation"]]
    [:li [:a {:href "https://bitbucket.org/peterk87/sistr_backend/wiki/Home" :target "_blank"}
          "SISTR backend server Git repository (Bitbucket)"]]
    [:li [:a {:href "https://bitbucket.org/peterk87/sistr-app/" :target "_blank"}
          "SISTR web app Git repository (Bitbucket)"]]
    [:li [:a {:href "https://github.com/peterk87/sistr_cmd" :target "_blank"}
          [:span "Standalone SISTR command-line app for offline serovar predictions"]]
     [:ul
      [:li [:code "pip install sistr-cmd"]]]]
    [:li [:a {:href "https://github.com/peterk87/sistr-packer" :target "_blank"}
          "SISTR Virtual Appliance build scripts"]]]])

(defn about-page []
  [:div.row
   [:div.col-md-3
    [:div.well.well-sm
     [web-browser-warning]
     [:div.well.well-sm
      [:h4 "App data status"]
      [app-data-status data/genome-name-metadata "Genome metadata"]
      [app-data-status data/serovar-prediction-results "Serovar prediction results"]
      [app-data-status data/tree-data "Phylogenetic tree data"]
      [app-data-status data/mst "Minimum spanning tree data"]]
     [serovar-variant-warning-panel]]]
   [:div.col-md-4
    [:h2 "SISTR: Salmonella " [:em "In Silico"] " Typing Resource"]
    [main-spiel]
    [links-of-interest]]
   [:div.col-md-3
    [sistr-news]
    [publications-of-interest]]])

