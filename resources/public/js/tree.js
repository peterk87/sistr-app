// Generated by LiveScript 1.2.0
(function(){
  var PhyloTree, out$ = typeof exports != 'undefined' && exports || this;
  out$.PhyloTree = PhyloTree = (function(){
    PhyloTree.displayName = 'PhyloTree';
    var prototype = PhyloTree.prototype, constructor = PhyloTree;
    prototype.get_plot_dim = function(){
      var w, h;
      w = this.options.width || d3.select(this.container_el_id).style('width') || d3.select(this.container_el_id).attr('width');
      h = this.options.height || d3.select(this.container_el_id).style('height') || d3.select(this.container_el_id).attr('height');
      w = parseInt(w);
      h = parseInt(h);
      this._width = w;
      return this._height = h;
    };
    prototype.get_tree = function(){
      var this$ = this;
      this.dist_sum_max = 0;
      return d3.layout.cluster().size([this._height, this._width]).children(function(it){
        return it.children;
      }).separation(function(a, b){
        var gs;
        gs = this$.options.genome_groups;
        return (gs[a.name].length + 1) + (gs[b.name].length + 1);
      });
    };
    function PhyloTree(container_el_id, tree_data, options){
      var leaf_dists, min_leaf_dist, max_leaf_dist, derp_dists, genome_group_sizes, res$, k, max_derp, dist_sum, this$ = this;
      this.container_el_id = container_el_id;
      this.tree_data = tree_data;
      this.options = options;
      this.get_pie_node_data = bind$(this, 'get_pie_node_data', prototype);
      this.container = d3.select(this.container_el_id);
      this.container$ = document.getElementById(this.container.attr('id'));
      this.get_plot_dim();
      this.margin = [20, 20, 100, 20];
      this._height = this._height - this.margin[1] - this.margin[3];
      this._width = this._width - this.margin[0] - this.margin[2];
      this._tree = this.get_tree();
      this.selected = [];
      this._diagonal = this.rightAngleDiagonal();
      this._nodes = this._tree(this.tree_data);
      this.xscale = this.scaleBranchLengths(this._nodes, this._width);
      leaf_dists = this._nodes.map(function(it){
        if (!it.children) {
          return it.rootDist;
        } else {
          return null;
        }
      });
      min_leaf_dist = d3.min(leaf_dists);
      max_leaf_dist = d3.max(leaf_dists);
      this._svg = d3.select(this.container_el_id).append("svg:svg").attr('width', this._width + this.margin[0] + this.margin[2]).attr('height', this._height + this.margin[1] + this.margin[3]);
      this._vis = this._svg.append("svg:g").attr('transform', "translate(20, 20)");
      derp_dists = this._nodes.map(function(it){
        if (!it.children) {
          return it.branch_length;
        } else {
          return null;
        }
      });
      res$ = [];
      for (k in this.options.genome_groups) {
        res$.push(this.options.genome_groups[k].length + 1);
      }
      genome_group_sizes = res$;
      max_derp = d3.max(genome_group_sizes);
      dist_sum = 2 * d3.sum(genome_group_sizes);
      this.r_y_scale = d3.scale.linear().domain([1, max_derp]).range([1 / dist_sum * this._height, max_derp / dist_sum * this._height]);
      this.yscale = d3.scale.linear().domain([0, this._height]).range([0, this._height]);
      this.zoom = d3.behavior.zoom().x(this.xscale).y(this.yscale).on('zoom', function(){
        this$.zoomed();
      });
      this._svg.call(this.zoom);
      this._link = this._vis.selectAll("path.link").data(this._tree.links(this._nodes)).enter().append("svg:path").attr('class', 'link').attr('d', this._diagonal).attr('fill', 'none').attr('stroke', 'black').attr('stroke-width', '1px');
      this._node = this._vis.selectAll("g.node").data(this._nodes).enter().append("svg:g").attr("class", function(it){
        if (it.children) {
          if (it.depth === 0) {
            return "root node";
          } else {
            return "inner node";
          }
        } else {
          return "leaf node";
        }
      }).attr("transform", function(it){
        return "translate(" + this$.xscale(it.rootDist) + "," + this$.yscale(it.x) + ")";
      });
      this.pie = d3.layout.pie().sort(null).value(function(it){
        return it.value;
      });
      this.arc = d3.svg.arc().outerRadius(function(it){
        var group_size;
        group_size = this$.options.genome_groups[it.data.name].length + 1;
        return this$.r_y_scale(group_size);
      }).innerRadius(0);
      if (!options.skipLabels) {
        this.leaves = this._vis.selectAll("g.leaf.node");
        this.leafPies = this.leaves.append("svg:g").attr('class', 'leaf-pie').attr('transform', function(it){
          return "translate(" + (5 + this$.r_y_scale(this$.options.genome_groups[it.name].length + 1)) + ",0)";
        }).on('click', function(d){
          var selected, evt;
          console.log("leaf-pies click", d);
          d.selected = !d.selected;
          selected = [];
          this$.leafPies.classed('selected', function(it){
            if (it.selected) {
              selected.push(it.name);
            }
            return it.selected;
          });
          this$.selected = selected;
          evt = new Event('change');
          this$.container$.dispatchEvent(evt);
        });
      }
    }
    prototype.zoomed = function(){
      var this$ = this;
      this._node.attr("transform", function(it){
        return "translate(" + this$.xscale(it.rootDist) + "," + this$.yscale(it.x) + ")";
      });
      this.arc = d3.svg.arc().outerRadius(function(it){
        var group_size;
        group_size = this$.options.genome_groups[it.data.name].length + 1;
        return this$.zoom.scale() * this$.r_y_scale(group_size);
      }).innerRadius(0);
      this.leafPies.attr('transform', function(it){
        return "translate(" + (5 + this$.zoom.scale() * this$.r_y_scale(this$.options.genome_groups[it.name].length + 1)) + ",0)";
      });
      this.leafPies.selectAll('path').attr('d', this.arc);
      this._link.attr('d', this._diagonal);
    };
    prototype.scaleBranchLengths = function(nodes, w){
      var visitPreOrder, rootDists, yscale;
      visitPreOrder = function(root, callback){
        var i$, ref$, child;
        callback(root);
        if (root.children) {
          for (i$ = (ref$ = root.children).length - 1; i$ >= 0; --i$) {
            child = ref$[i$];
            visitPreOrder(child, callback);
          }
        }
      };
      visitPreOrder(nodes[0], function(node){
        node.rootDist = (node.parent ? node.parent.rootDist : 0) + (node.branch_length || 0);
      });
      rootDists = nodes.map(function(it){
        return it.rootDist;
      });
      yscale = d3.scale.linear().domain([0, d3.max(rootDists)]).range([0, w]);
      return yscale;
    };
    prototype.rightAngleDiagonal = function(){
      var diagonal, projection, path, this$ = this;
      diagonal = function(it){
        var source, target, pathData, p;
        source = it.source;
        target = it.target;
        pathData = [
          source, {
            x: target.x,
            rootDist: source.rootDist
          }, target
        ];
        pathData = pathData.map(projection);
        p = path(pathData);
        return p;
      };
      projection = function(it){
        return [this$.xscale(it.rootDist), this$.yscale(it.x)];
      };
      path = function(it){
        return "M" + it[0] + " " + it[1] + " " + it[2];
      };
      return diagonal;
    };
    prototype.highlight_leaves = function(leaves){
      if (leaves == null) {
        return;
      }
      this.leaves.classed('table-selected', function(it){
        return leaves.indexOf(it.name !== -1);
      });
    };
    prototype.clear_selection = function(){
      var evt;
      this.selected = [];
      this.leafPies.classed('selected', function(it){
        it.selected = false;
        return false;
      });
      evt = new Event('change');
      this.container$.dispatchEvent(evt);
    };
    prototype.get_pie_node_data = function(d, category){
      var strains, category_types, i$, len$, strain, strain_metadata, leftovers, type_counts, type, count;
      category == null && (category = 'source_type');
      strains = [d.name].concat(this.options.genome_groups[d.name]);
      category_types = [];
      for (i$ = 0, len$ = strains.length; i$ < len$; ++i$) {
        strain = strains[i$];
        strain_metadata = this.options.md[strain];
        if (strain_metadata == null) {
          console.log('genome', strain, strain_metadata);
        } else {
          category_types.push(strain_metadata[category]);
        }
      }
      leftovers = {
        'leftovers': 0
      };
      type_counts = _.countBy(category_types);
      for (type in type_counts) {
        count = type_counts[type];
        if (type in this.group_colour_dict) {
          leftovers[type] = count;
        } else {
          leftovers['leftovers'] = leftovers['leftovers'] + count;
        }
      }
      return this.pie((function(){
        var ref$, results$ = [];
        for (type in ref$ = leftovers) {
          count = ref$[type];
          results$.push({
            value: count,
            type: type,
            name: d.name
          });
        }
        return results$;
      }()));
    };
    prototype.get_group_colour = function(group){
      if (group in this.group_colour_dict) {
        return this.group_colour_dict[group];
      }
      return '#ddd';
    };
    prototype.update_highlighted_groups = function(category, groups, colours){
      var i$, len$, i, d, leaf_paths, this$ = this;
      this.update_legend(category, groups, colours);
      this.group_colour_dict = {};
      for (i$ = 0, len$ = groups.length; i$ < len$; ++i$) {
        i = i$;
        d = groups[i$];
        this.group_colour_dict[d] = colours[i];
      }
      leaf_paths = this.leafPies.selectAll('path').data(function(it){
        return this$.get_pie_node_data(it, category);
      }, function(it){
        return it.data.type;
      });
      leaf_paths.enter().append('path').attr('d', this.arc).attr('fill', function(it){
        return this$.get_group_colour(it.data.type);
      });
      leaf_paths.attr('d', this.arc).attr('fill', function(it){
        return this$.get_group_colour(it.data.type);
      });
      leaf_paths.exit().remove();
    };
    prototype.update_legend = function(title, groups, colours){
      var legend_data, datum, i$, to$, i, legend, legend_background, legend_items, legend_dim;
      if (groups === null || colours === null || groups.length === 0 || colours.length === 0) {
        this._svg.selectAll('.legend').transition().style('fill-opacity', 0).remove();
        this._svg.selectAll('.legend-title').remove();
      } else {
        legend_data = [];
        /*
        Check if single group specified
        */
        if (typeof groups === 'string') {
          datum = {
            group: groups,
            colour: colours
          };
          legend_data.push(datum);
        } else {
          /*
          If more than one group is highlighted
          */
          for (i$ = 0, to$ = groups.length; i$ < to$; ++i$) {
            i = i$;
            datum = {
              group: groups[i],
              colour: colours[i]
            };
            legend_data.push(datum);
          }
        }
        /*
        Remove the old legend
        */
        this._svg.selectAll('.legend').remove();
        /*
        Create new legend
        */
        legend = this._svg.append('g').attr('class', 'legend');
        legend_background = legend.append('rect').attr('fill', 'white').attr('fill-opacity', 0.8).style('stroke', 'lightgray').style('stroke-width', '2px');
        legend_items = legend.selectAll('.legend-item').data(legend_data, function(it){
          return it.group;
        });
        legend_items = legend_items.enter().append('g').attr('class', 'legend-item').attr('transform', function(d, i){
          return "translate(0," + ((i + 1) * 20 + 10) + ")";
        });
        /*
        Create rect elements with group colours
        */
        legend_items.append('rect').attr('x', 0).attr('width', 18).attr('height', 18).style('fill', function(it){
          return it.colour;
        });
        /*
        Create text elements with group names
        */
        legend_items.append('text').attr('x', 22).attr('y', 9).attr('dy', ".35em").style('text-anchor', 'start').text(function(it){
          return it.group;
        });
        /*
        Update the legend title
        */
        legend.selectAll('.legend-title').remove();
        legend.append('text').attr('class', 'legend-title').attr('x', 22).attr('y', 20).attr('font-weight', 'bold').text(title);
        /*
        Get the width of the legend g element
        */
        legend_dim = legend[0][0].getBBox();
        legend_background.attr('x', -5).attr('width', legend_dim.width + 10).attr('height', legend_dim.height + 20);
        /*
        Translate the legend group over to the right side of the plot area
        */
        legend.attr('transform', "translate(" + (this._width - legend_dim.width - 10 + this.margin[2]) + ", 10)");
      }
    };
    return PhyloTree;
  }());
  function bind$(obj, key, target){
    return function(){ return (target || obj)[key].apply(obj, arguments) };
  }
}).call(this);
