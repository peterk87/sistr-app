objectify_metadata = (data) ->
  md = {}
  for name, i in data.name
    tmp = {}
    for k, v of data
      val = v[i]
      if val?
        tmp[k] = v[i]
    md[name] = tmp
  return md


/*
Minimum Spanning Tree using D3 Force-Directed Graph

Construct a D3 MST visualization using source to target linkage information.

*/
export class MST
  
  _tooltip_visible : false
  _width : 800
  _height : 600
  group_colour_dict: {}
  _sticky_nodes : false


  (@el, @_width=800, @_height=600) ->
    @pie := d3.layout.pie!
      .sort null
      .value ->
        it.value

    @arc := d3.svg.arc!
      .outerRadius ~>
        other_genomes = @data.grouped_nodes[it.data.idx]
        10 + Math.pow other_genomes.length, 0.8
      .innerRadius 0

    d3.select @el .select \svg .remove!

    @_svg := d3.select @el
      .append \svg

    @_background_rect := @_svg.append \rect
      .attr \pointer-events \all
      .style \fill \none
      .style \stroke \lightgray
      .style \stroke-width \2px
      .call do
        d3.behavior.zoom! .on \zoom !~>
          @_visualization.attr \transform "translate(#{d3.event.translate}) scale(#{d3.event.scale})"

    @_visualization := @_svg.append \g

    @_d3_force_directed_graph := d3.layout.force!
    _this = @
    @_force_drag := @_d3_force_directed_graph.drag!
      .on \dragstart !-> 
        if _this._sticky_nodes 
          it.fixed = true

          d3.select @
            .classed \fixed, true
          
          console.log "dragstart", @, it

    @force_nodes := @_d3_force_directed_graph.nodes!
    @force_links := @_d3_force_directed_graph.links!

    @_update_svg_size!

    # on window resize re-initialize the size of the SVG plot
    $ window .resize !~> @_update_svg_size!




  _update_svg_size: !->
    @_width := $ @el .width!
    # @_height := $ window .height! - 200

    @_d3_force_directed_graph.size [@_width, @_height]
    @_svg
      .attr \width @_width
      .attr \height @_height
    @_background_rect
      .attr \height @_height
      .attr \width @_width
  
  /**
   * Add a new node object with a specified ID and fill colour. If the node 
   * exists then update the colour of the node.
   * @param  {String} id     Node ID
   * @return {Boolean}       If new node has been created
  */
  _add_node: (id) ->
    nodeMatch = @_find_node id
    if nodeMatch?
      # nodeMatch.colour = colour
      # nodeMatch.size = size
      return false
    else
      @force_nodes.push do
        id: id
        value: -1
        selected: false
        # colour: colour
        # size: size
      return true

  /**
   * Update the D3 force-directed graph nodes. The incoming data determines
   * whether an existing node is removed or kept and if a node should be created
   * if it currently does not exist.
   * @param  {Array String} ids     Node IDs
   * @return {Array Object}         D3 force-directed nodes object array.
  */
  update_nodes: (ids) ->
    /*
    Create set to keep track of nodes that should be in the plot. Any nodes that
    are currently in the plot and shouldn't be will be removed. 
    */
    node_id_set = {}
    /*
    Create new nodes
    */
    for i from 0 til ids.length
      id = ids[i]
      node_id_set[id] = true
      @_add_node id

    /*
    Remove any nodes that are not needed. If a node doesn't exist in 
    node_id_set then remove it from the plot
    */
    for i in [0 til @force_nodes.length] by -1
      n = @force_nodes[i]
      if n.id not of node_id_set
        /*
        Remove node and any associated links to that node.
        */
        @_remove_node_at i
    
    return @force_nodes

  /**
   * Remove D3 force-directed graph node and associated links at the specified
   * node index.
   * @param  {Integer} idx Node index
   * @return {Object}     Removed node object
  */
  _remove_node_at: (idx) ->
    n = @force_nodes[idx]
    for i in [0 til @force_links.length] by -1
      x = @force_links[i]
      if x.source.id is n.id or x.target.id is n.id
        @force_links.splice i, 1
    @force_nodes.splice idx, 1
    

  /**
   * Remove link with specified source and target node IDs from D3 force-
   * directed graph links array. 
   * @param  {String} source Source node ID
   * @param  {String} target Target node ID
   * @return {Object}        Link object that has been removed or null if 
   *                         nothing has been removed
  */
  _remove_link: (source, target) ->
    for i in [0 til @force_links.length] by -1
      if @force_links[i].source.id == source and @force_links[i].target.id == target
        return @force_links.splice i, 1
    return null

  /**
   * Add a new link between specified source and target nodes with a distance
   * value.
   * @param {String} source Source node ID
   * @param {String} target Target node ID
   * @param {Integer} value  Link distance
   * @return {Object}
  */
  _add_link: (source, target, value) ->
    @force_links.push do
      source: @_find_node source
      target: @_find_node target
      value: value

  /**
   * Substitute node indices with node string IDs in links object array
   * @param  {Array Object} links An array of link objects with integer indices
   *                        for the sources and targets as well as distance info
   * @param  {Array String} ids   Node IDs. Same order as links source and 
   *                        target indices.
   * @return {Array Object}       An array of link objects with string IDs for
   *                              the sources and targets as well as distance
   *                              info.
  */
  _objectify_link_data: (links, ids) ->
    link_data = []
    i = 0
    for i from 0 til links.length
      x = links[i]
      source = ids[x.source]
      target = ids[x.target]
      link_data.push do
        source: source
        target: target
        value: x.distances
    return link_data

  /**
   * Update the links within the MST visualization with the new incoming data.
   * Unnecessary links are pruned, existing needed links are preserved and new 
   * links are created.
   * @param  {Array Object} links An array of link objects with integer indices
   *                        for the sources and targets as well as distance info
   * @param  {Array String} ids   Node IDs. Same order as links source and 
   *                        target indices.
   * @return {Array Object}       D3 Force-directed graph links for the MST
   *                              i.e. @force_links
  */
  update_links: (links, ids) ->
    # since this function receives an array of link data in the form:
    # link = 
    #  target: integer referring to index of target id
    #  source: integer referring to index of source id
    #  distances: integer
    # and the the node ids as an array of id Strings, we want to substitute in 
    # the id names for the nodes for the integer numbers
    # This is done because R produces integer indices to the sources and targets
    new_link_data = @_objectify_link_data links, ids
    orig_links = {}
    # determine which links already exist and keep track of them in a hash set
    # orig_links so that only truly new links are added and unnecessary links
    # are removed
    for i from 0 til @force_links.length
      x = @force_links[i]
      key_1 = "#{x.source.id}-#{x.target.id}"
      key_2 = "#{x.target.id}-#{x.source.id}"
      unless (key_1 of orig_links or key_2 of orig_links)
        orig_links[key_1] = true
    link_set = {}
    # add new links and account for existing links
    for i from 0 til new_link_data.length
      x = new_link_data[i]
      key_1 = "#{x.source}-#{x.target}"
      key_2 = "#{x.target}-#{x.source}"
      unless (key_1 of orig_links 
        or key_2 of orig_links 
        or key_1 of link_set 
        or key_2 of link_set)
        link_set[key_1] = true
        @_add_link x.source, x.target, x.value
      # if the link already exists then there is no need to add it again but we
      # want to make sure that it's accounted for prior to the link pruning step
      if key_1 of orig_links
        link_set[key_1] = true
    # remove unnecessary links
    for i from @force_links.length - 1 to 0 by -1
      x = @force_links[i]
      key_1 = "#{x.source.id}-#{x.target.id}"
      key_2 = "#{x.target.id}-#{x.source.id}"
      unless key_1 of link_set or key_2 of link_set
        @_remove_link x.source.id, x.target.id

    return @force_links

  /**
   * Find node with specified id and return the ref to the object or null if not
   * found.
   * @param  {String} id Node object id
   * @return {Object}    Node object or null if not found
  */
  _find_node: (id) ->
    for i from 0 til @force_nodes.length
      n = @force_nodes[i]
      return n if n.id is id
    return null

  get_pie_node_data: (d, category='source_type') ~>
    strains = [d.id] ++ @data.grouped_nodes[d.id]
    category_types = []
    for strain in strains
      strain_metadata = @_metadata[strain]
      unless strain_metadata?
        console.log 'genome', strain, strain_metadata
      else
        category_types.push strain_metadata[category]

    leftovers = {'leftovers':0}
    type_counts = _.countBy category_types
    for type, count of type_counts
      if type of @group_colour_dict
        leftovers[type] = count
      else
        leftovers['leftovers'] = leftovers['leftovers'] + count
    @pie [{value:count, type:type, idx:d.id} for type, count of leftovers]

  get_group_colour: (group) ->
    if group of @group_colour_dict
      return @group_colour_dict[group]
    '#ddd'


  update_highlighted_groups: (category, groups, colours) !->
    @update_legend category, groups, colours
    @group_colour_dict := {}
    for d, i in groups
      @group_colour_dict[d] = colours[i]

    nodes_existing = @_node_elements.selectAll \path
      .data (~> @get_pie_node_data it, category), ( -> it.data.type)

    nodes_existing.enter!
      .append \path
      .attr \d @arc
      .attr \fill ~>
        @get_group_colour it.data.type

    nodes_existing
      .attr \d @arc
      .attr \fill ~>
        @get_group_colour it.data.type

    nodes_existing.exit!remove!

    @_node_elements.exit!
      .transition!
      .style \opacity 0
      .remove!

    @_node_elements.selectAll \text .remove!

    node_text = @_node_elements.append \text
      .attr \class \node-group-number
      .attr \dy ".71em"
      .attr \text-anchor \middle
      .attr \font-size ~>
        font_size = 6 + Math.pow @data.grouped_nodes[it.id].length, @size_power
        "#{font_size}px"
      .attr \font-family \sans-serif
      .attr \transform ~>
        font_size = 6 + Math.pow @data.grouped_nodes[it.id].length, @size_power
        "translate(0,#{-font_size/3})"
      .text ~> 
        1 + @data.grouped_nodes[it.id].length

    @node_text := node_text
    
    @node_text.style \opacity if @_show_node_sizes then 1 else 0


  clear_selection: !->
    @selected := []
    for node in @force_nodes
      node.selected = false
    @_node_elements.classed \selected, false
    e = new Event \change
    @el.dispatchEvent e

  _node_mouse_click: (d) !->
    d.selected = !d.selected
    @_node_elements.classed \selected, -> it.selected
    @selected := [node.id for node in @force_nodes when node.selected]
    e = new Event \change
    @el.dispatchEvent e
    # console.log 'node clicked', d, d.id

  /**
   * Update MST with new data.
  */
  update: (data) !->
    unless data?
      return

    @data := data

    unless @size_power?
      @size_power := 0.8

    if @data.group_colours? and @data.groups?
      @group_colour_dict := {}
      for d, i in @data.groups
        @group_colour_dict[d] = @data.group_colours[i]

    @_link_elements := @_visualization.selectAll \g.link
      .data @force_links, -> "#{it.source.id}-#{it.target.id}"
    
    link_enter = @_link_elements.enter!.append \g 
      .attr \id, ->
        "#{it.source.id}-#{it.target.id}"
      .attr \class "link mst-element"
    link_enter.append \line
      .style \stroke \black
      .style \stroke-width  -> 
        "#{5 * (1 - (it.value / data.max_distance))}px"

    link_enter.append \text
      .attr \class \distance-label
      .attr \dy ".71em"
      .attr \text-anchor \middle
      .attr \font-size \10px
      .attr \font-family \sans-serif
      .style \fill \white
      .style \stroke \black
      .style \stroke-width \.25px
      .style \opacity 0
      .text -> it.value

    @_link_elements.selectAll \.distance-label
      .transition!
      .style \opacity if @data.show_distances then 1 else 0
      .text -> it.value


    @_link_elements.exit!
      .transition!
      .style \opacity 0
      .remove!
    
    @_node_elements := @_visualization.selectAll \.node
      .data @force_nodes, -> it.id
    
    new_node_elements = @_node_elements.enter!.append \g
      .attr \class "node mst-element"
      .attr \id -> it.id
      .attr \title -> it.id

    _this = @
    new_node_elements.call @_force_drag
      .on \dblclick !-> 
        if _this._sticky_nodes
          d3.select @ 
            .classed \fixed !-> 
              it.fixed = false
              false
      .on \click !~> @_node_mouse_click it
      # .on \mouseover !~> @_node_mouseover_event it
      # .on \mouseout !~> @_node_mouseout_event it

    # @_metadata := objectify_metadata @data.md
    @_metadata := @data.md

    paths = new_node_elements.selectAll \path
      .data (~> @get_pie_node_data it), ( -> it.data.type)
      .enter!
      .append \path
      .attr \d @arc
      .attr \fill ~>
        @get_group_colour it.data.type

    nodes_existing = @_node_elements.selectAll \path
      .data (~> @get_pie_node_data it), ( -> it.data.type)

    nodes_existing.enter!
      .append \path
      .attr \d @arc
      .attr \fill ~>
        @get_group_colour it.data.type

    nodes_existing
      .attr \d @arc
      .attr \fill ~>
        @get_group_colour it.data.type

    nodes_existing.exit!remove!

    @_node_elements.exit!
      .transition!
      .style \opacity 0
      .remove!


    @_d3_force_directed_graph.on \tick !~>
      @_link_elements.selectAll \text
        .attr \x -> (it.source.x + it.target.x) / 2.0
        .attr \y -> (it.source.y + it.target.y) / 2.0

      @_link_elements.selectAll \line
        .attr \x1 -> it.source.x
        .attr \y1 -> it.source.y
        .attr \x2 -> it.target.x
        .attr \y2 -> it.target.y

      @_node_elements.attr \transform -> "translate(#{it.x},#{it.y})"
    

    @_d3_force_directed_graph
      .gravity if @data.gravity? then @data.gravity else 0.05
      .distance if @data.distance? then @data.distance else 50
      .charge if @data.charge? then @data.charge else -120
      .size [@_width, @_height]

    if @data.paused
      @_d3_force_directed_graph.stop!
    else
      @_d3_force_directed_graph.start!

    # bring nodes to the top so that they appear overtop of the line links
    /*
    The nodes need to be brought in front of the links so both links and nodes
    are classed mst-element. Nodes have a value of -1 while links have a value
    of 0 or greater so if the 
    */
    @_svg.selectAll \.mst-element 
      .sort (a, b) -> d3.descending a.value, b.value

    
  show_link_distances: (@show_distances) !->
    console.log 'show_link_distances', @show_distances
    @_link_elements.selectAll \.distance-label
      .transition!
      .style \opacity if @show_distances then 1 else 0
      .text -> it.value

  show_node_sizes: (@_show_node_sizes) !->
    @node_text.style \opacity if @_show_node_sizes then 1 else 0

  update_node_sizes: (size_power) !->
    @size_power := size_power
    @arc := d3.svg.arc!
      .outerRadius ~>
        other_genomes = @data.grouped_nodes[it.data.idx]
        10 + Math.pow other_genomes.length, @size_power
      .innerRadius 0
    @_node_elements.selectAll \path 
      .transition!
      .attr \d @arc
    @_node_elements.selectAll \text
      .transition!
      .attr \font-size ~>
        font_size = 6 + Math.pow @data.grouped_nodes[it.id].length, @size_power
        "#{font_size}px"
      


  start_force: !->
    @_d3_force_directed_graph.start!

  stop_force: !->
    @_d3_force_directed_graph.stop!


  /**
   * Update/generate the legend for the scatterplot showing the groups being
   * highlighted in the plot.
   * This function needs to be called separately from the update_data function.
   * @param  {String}       title         Legend title
   * @param  {String Array} groups        The ids of the groups being 
   *                        highlighted in the plot.
   * @param  {String Array} group_colours The HTML colours for the groups being 
   *                        highlighted in the plot.
  */
  update_legend: (title, groups, group_colours) !->
    if groups is null or group_colours is null or groups.length is 0 or group_colours.length is 0
      # remove legend if there are no groups highlighted
      @_svg.selectAll \.legend
        .transition!
        .style \fill-opacity 0
        .remove!
      @_svg.selectAll \.legend-title 
        .remove!
    else #if groups.length == group_colours.length
      # update legend with new group data and colours
      legend_data = []
      
      /*
      Check if single group specified
      */
      if typeof groups is \string
        datum =
          group: groups
          group_colour: group_colours
        legend_data.push datum
      else
        /*
        If more than one group is highlighted
        */
        for i til groups.length
          datum =
            group: groups[i]
            group_colour: group_colours[i]
          legend_data.push datum

      /*
      Remove the old legend
      */
      @_svg.selectAll \.legend .remove!

      /*
      Create new legend
      */
      legend = @_svg
        .append \g
        .attr \class \legend

      legend_background = legend.append \rect
        .attr \fill \white
        .attr \fill-opacity 0.8
        .style \stroke \lightgray
        .style \stroke-width \2px
      


      legend_items = legend.selectAll \.legend-item
        .data legend_data, -> it.group

      legend_items = legend_items.enter!append \g
        .attr \class \legend-item
        .attr \transform (d, i) -> 
          "translate(0,#{(i+1) * 20 + 10})"

      /*
      Create rect elements with group colours
      */
      legend_items.append \rect 
        .attr \x 0
        .attr \width 18
        .attr \height 18
        .style \fill -> it.group_colour
      
      /*
      Create text elements with group names
      */
      legend_items.append \text
        .attr \x 22
        .attr \y 9
        .attr \dy ".35em"
        .style \text-anchor \start
        .text -> it.group

      /*
      Update the legend title
      */
      legend.selectAll \.legend-title .remove!
      legend.append \text
        .attr \class \legend-title
        .attr \x 22
        .attr \y 20
        .attr \font-weight \bold
        .text title

      /*
      Get the width of the legend g element
      */
      legend_dim = legend[0][0].getBBox!
      
      legend_background
        .attr \x -5
        .attr \width legend_dim.width + 10
        .attr \height legend_dim.height + 5

      /*
      Translate the legend group over to the right side of the plot area
      */
      legend.attr \transform, "translate(#{@_width - legend_dim.width - 10}, 10)"

  enable_sticky_nodes: (@_sticky_nodes) !->
    unless @_sticky_nodes
      for n in @force_nodes
        n.fixed = false
      @_node_elements.classed \fixed, false

  fix_all_nodes: !->
    for n in @force_nodes
      n.fixed = true
    @_node_elements.classed \fixed, true