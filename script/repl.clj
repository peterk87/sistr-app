;TODO: debug error when trying to use figwheel this way as recommended by https://github.com/bhauman/lein-figwheel/wiki/Running-figwheel-in-a-Cursive-Clojure-REPL
(use 'figwheel-sidecar.repl-api)
(start-figwheel! {:all-builds (figwheel-sidecar.repl/get-project-cljs-builds)})
(cljs-repl)