var L = {
    "version": {},
    "noConflict": function () {},
    "Util": {
        "extend": function () {},
        "bind": function () {},
        "stamp": function () {},
        "invokeEach": function () {},
        "limitExecByInterval": function () {},
        "falseFn": function () {},
        "formatNum": function () {},
        "trim": function () {},
        "splitWords": function () {},
        "setOptions": function () {},
        "getParamString": function () {},
        "template": function () {},
        "isArray": function () {},
        "emptyImageUrl": {},
        "requestAnimFrame": function () {},
        "cancelAnimFrame": function () {}
    },
    "extend": function () {},
    "bind": function () {},
    "stamp": function () {},
    "setOptions": function () {},
    "Class": function () {},
    "Mixin": {
        "Events": {
            "addEventListener": function () {},
            "hasEventListeners": function () {},
            "removeEventListener": function () {},
            "clearAllEventListeners": function () {},
            "fireEvent": function () {},
            "addOneTimeEventListener": function () {},
            "on": function () {},
            "off": function () {},
            "once": function () {},
            "fire": function () {}
        }
    },
    "Browser": {
        "ie": {},
        "ielt9": {},
        "webkit": {},
        "gecko": {},
        "android": {},
        "android23": {},
        "chrome": {},
        "ie3d": {},
        "webkit3d": {},
        "gecko3d": {},
        "opera3d": {},
        "any3d": {},
        "mobile": {},
        "mobileWebkit": {},
        "mobileWebkit3d": {},
        "mobileOpera": {},
        "touch": {},
        "msPointer": {},
        "pointer": {},
        "retina": {},
        "svg": {},
        "vml": {},
        "canvas": {}
    },
    "Point": function () {},
    "point": function () {},
    "Bounds": function () {},
    "bounds": function () {},
    "Transformation": function () {},
    "DomUtil": {
        "get": function () {},
        "getStyle": function () {},
        "getViewportOffset": function () {},
        "documentIsLtr": function () {},
        "create": function () {},
        "hasClass": function () {},
        "addClass": function () {},
        "removeClass": function () {},
        "_setClass": function () {},
        "_getClass": function () {},
        "setOpacity": function () {},
        "testProp": function () {},
        "getTranslateString": function () {},
        "getScaleString": function () {},
        "setPosition": function () {},
        "getPosition": function () {},
        "TRANSFORM": {},
        "TRANSITION": {},
        "TRANSITION_END": {},
        "disableTextSelection": function () {},
        "enableTextSelection": function () {},
        "disableImageDrag": function () {},
        "enableImageDrag": function () {}
    },
    "LatLng": function () {},
    "latLng": function () {},
    "LatLngBounds": function () {},
    "latLngBounds": function () {},
    "Projection": {
        "SphericalMercator": {
            "MAX_LATITUDE": {},
            "project": function () {},
            "unproject": function () {}
        },
        "LonLat": {
            "project": function () {},
            "unproject": function () {}
        },
        "Mercator": {
            "MAX_LATITUDE": {},
            "R_MINOR": {},
            "R_MAJOR": {},
            "project": function () {},
            "unproject": function () {}
        }
    },
    "CRS": {
        "latLngToPoint": function () {},
        "pointToLatLng": function () {},
        "project": function () {},
        "scale": function () {},
        "getSize": function () {},
        "Simple": {
            "latLngToPoint": function () {},
            "pointToLatLng": function () {},
            "project": function () {},
            "scale": function () {},
            "getSize": function () {},
            "projection": {
                "project": function () {},
                "unproject": function () {}
            },
            "transformation": {
                "_a": {},
                "_b": {},
                "_c": {},
                "_d": {},
                "transform": function () {},
                "_transform": function () {},
                "untransform": function () {}
            }
        },
        "EPSG3857": {
            "latLngToPoint": function () {},
            "pointToLatLng": function () {},
            "project": function () {},
            "scale": function () {},
            "getSize": function () {},
            "Simple": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "projection": {
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "code": {},
            "projection": {
                "MAX_LATITUDE": {},
                "project": function () {},
                "unproject": function () {}
            },
            "transformation": {
                "_a": {},
                "_b": {},
                "_c": {},
                "_d": {},
                "transform": function () {},
                "_transform": function () {},
                "untransform": function () {}
            }
        },
        "EPSG900913": {
            "latLngToPoint": function () {},
            "pointToLatLng": function () {},
            "project": function () {},
            "scale": function () {},
            "getSize": function () {},
            "Simple": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "projection": {
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "code": {},
            "projection": {
                "MAX_LATITUDE": {},
                "project": function () {},
                "unproject": function () {}
            },
            "transformation": {
                "_a": {},
                "_b": {},
                "_c": {},
                "_d": {},
                "transform": function () {},
                "_transform": function () {},
                "untransform": function () {}
            }
        },
        "EPSG4326": {
            "latLngToPoint": function () {},
            "pointToLatLng": function () {},
            "project": function () {},
            "scale": function () {},
            "getSize": function () {},
            "Simple": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "projection": {
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "EPSG3857": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "Simple": {
                    "latLngToPoint": function () {},
                    "pointToLatLng": function () {},
                    "project": function () {},
                    "scale": function () {},
                    "getSize": function () {},
                    "projection": {
                        "project": function () {},
                        "unproject": function () {}
                    },
                    "transformation": {
                        "_a": {},
                        "_b": {},
                        "_c": {},
                        "_d": {},
                        "transform": function () {},
                        "_transform": function () {},
                        "untransform": function () {}
                    }
                },
                "code": {},
                "projection": {
                    "MAX_LATITUDE": {},
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "EPSG900913": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "Simple": {
                    "latLngToPoint": function () {},
                    "pointToLatLng": function () {},
                    "project": function () {},
                    "scale": function () {},
                    "getSize": function () {},
                    "projection": {
                        "project": function () {},
                        "unproject": function () {}
                    },
                    "transformation": {
                        "_a": {},
                        "_b": {},
                        "_c": {},
                        "_d": {},
                        "transform": function () {},
                        "_transform": function () {},
                        "untransform": function () {}
                    }
                },
                "code": {},
                "projection": {
                    "MAX_LATITUDE": {},
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "code": {},
            "projection": {
                "project": function () {},
                "unproject": function () {}
            },
            "transformation": {
                "_a": {},
                "_b": {},
                "_c": {},
                "_d": {},
                "transform": function () {},
                "_transform": function () {},
                "untransform": function () {}
            }
        },
        "EPSG3395": {
            "latLngToPoint": function () {},
            "pointToLatLng": function () {},
            "project": function () {},
            "scale": function () {},
            "getSize": function () {},
            "Simple": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "projection": {
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "EPSG3857": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "Simple": {
                    "latLngToPoint": function () {},
                    "pointToLatLng": function () {},
                    "project": function () {},
                    "scale": function () {},
                    "getSize": function () {},
                    "projection": {
                        "project": function () {},
                        "unproject": function () {}
                    },
                    "transformation": {
                        "_a": {},
                        "_b": {},
                        "_c": {},
                        "_d": {},
                        "transform": function () {},
                        "_transform": function () {},
                        "untransform": function () {}
                    }
                },
                "code": {},
                "projection": {
                    "MAX_LATITUDE": {},
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "EPSG900913": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "Simple": {
                    "latLngToPoint": function () {},
                    "pointToLatLng": function () {},
                    "project": function () {},
                    "scale": function () {},
                    "getSize": function () {},
                    "projection": {
                        "project": function () {},
                        "unproject": function () {}
                    },
                    "transformation": {
                        "_a": {},
                        "_b": {},
                        "_c": {},
                        "_d": {},
                        "transform": function () {},
                        "_transform": function () {},
                        "untransform": function () {}
                    }
                },
                "code": {},
                "projection": {
                    "MAX_LATITUDE": {},
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "EPSG4326": {
                "latLngToPoint": function () {},
                "pointToLatLng": function () {},
                "project": function () {},
                "scale": function () {},
                "getSize": function () {},
                "Simple": {
                    "latLngToPoint": function () {},
                    "pointToLatLng": function () {},
                    "project": function () {},
                    "scale": function () {},
                    "getSize": function () {},
                    "projection": {
                        "project": function () {},
                        "unproject": function () {}
                    },
                    "transformation": {
                        "_a": {},
                        "_b": {},
                        "_c": {},
                        "_d": {},
                        "transform": function () {},
                        "_transform": function () {},
                        "untransform": function () {}
                    }
                },
                "EPSG3857": {
                    "latLngToPoint": function () {},
                    "pointToLatLng": function () {},
                    "project": function () {},
                    "scale": function () {},
                    "getSize": function () {},
                    "Simple": {
                        "latLngToPoint": function () {},
                        "pointToLatLng": function () {},
                        "project": function () {},
                        "scale": function () {},
                        "getSize": function () {},
                        "projection": {
                            "project": function () {},
                            "unproject": function () {}
                        },
                        "transformation": {
                            "_a": {},
                            "_b": {},
                            "_c": {},
                            "_d": {},
                            "transform": function () {},
                            "_transform": function () {},
                            "untransform": function () {}
                        }
                    },
                    "code": {},
                    "projection": {
                        "MAX_LATITUDE": {},
                        "project": function () {},
                        "unproject": function () {}
                    },
                    "transformation": {
                        "_a": {},
                        "_b": {},
                        "_c": {},
                        "_d": {},
                        "transform": function () {},
                        "_transform": function () {},
                        "untransform": function () {}
                    }
                },
                "EPSG900913": {
                    "latLngToPoint": function () {},
                    "pointToLatLng": function () {},
                    "project": function () {},
                    "scale": function () {},
                    "getSize": function () {},
                    "Simple": {
                        "latLngToPoint": function () {},
                        "pointToLatLng": function () {},
                        "project": function () {},
                        "scale": function () {},
                        "getSize": function () {},
                        "projection": {
                            "project": function () {},
                            "unproject": function () {}
                        },
                        "transformation": {
                            "_a": {},
                            "_b": {},
                            "_c": {},
                            "_d": {},
                            "transform": function () {},
                            "_transform": function () {},
                            "untransform": function () {}
                        }
                    },
                    "code": {},
                    "projection": {
                        "MAX_LATITUDE": {},
                        "project": function () {},
                        "unproject": function () {}
                    },
                    "transformation": {
                        "_a": {},
                        "_b": {},
                        "_c": {},
                        "_d": {},
                        "transform": function () {},
                        "_transform": function () {},
                        "untransform": function () {}
                    }
                },
                "code": {},
                "projection": {
                    "project": function () {},
                    "unproject": function () {}
                },
                "transformation": {
                    "_a": {},
                    "_b": {},
                    "_c": {},
                    "_d": {},
                    "transform": function () {},
                    "_transform": function () {},
                    "untransform": function () {}
                }
            },
            "code": {},
            "projection": {
                "MAX_LATITUDE": {},
                "R_MINOR": {},
                "R_MAJOR": {},
                "project": function () {},
                "unproject": function () {}
            },
            "transformation": {
                "_a": {},
                "_b": {},
                "_c": {},
                "_d": {},
                "transform": function () {},
                "_transform": function () {},
                "untransform": function () {}
            }
        }
    },
    "Map": function () {},
    "map": function () {},
    "TileLayer": function () {},
    "tileLayer": function () {},
    "ImageOverlay": function () {},
    "imageOverlay": function () {},
    "Icon": function () {},
    "icon": function () {},
    "Marker": function () {},
    "marker": function () {},
    "DivIcon": function () {},
    "divIcon": function () {},
    "Popup": function () {},
    "popup": function () {},
    "LayerGroup": function () {},
    "layerGroup": function () {},
    "FeatureGroup": function () {},
    "featureGroup": function () {},
    "Path": function () {},
    "LineUtil": {
        "simplify": function () {},
        "pointToSegmentDistance": function () {},
        "closestPointOnSegment": function () {},
        "_simplifyDP": function () {},
        "_simplifyDPStep": function () {},
        "_reducePoints": function () {},
        "clipSegment": function () {},
        "_getEdgeIntersection": function () {},
        "_getBitCode": function () {},
        "_sqDist": function () {},
        "_sqClosestPointOnSegment": function () {}
    },
    "Polyline": function () {},
    "polyline": function () {},
    "PolyUtil": {
        "clipPolygon": function () {}
    },
    "Polygon": function () {},
    "polygon": function () {},
    "MultiPolyline": function () {},
    "MultiPolygon": function () {},
    "multiPolyline": function () {},
    "multiPolygon": function () {},
    "Rectangle": function () {},
    "rectangle": function () {},
    "Circle": function () {},
    "circle": function () {},
    "CircleMarker": function () {},
    "circleMarker": function () {},
    "GeoJSON": function () {},
    "geoJson": function () {},
    "DomEvent": {
        "addListener": function () {},
        "removeListener": function () {},
        "stopPropagation": function () {},
        "disableScrollPropagation": function () {},
        "disableClickPropagation": function () {},
        "preventDefault": function () {},
        "stop": function () {},
        "getMousePosition": function () {},
        "getWheelDelta": function () {},
        "_skipEvents": function () {},
        "_fakeStop": function () {},
        "_skipped": function () {},
        "_checkMouse": function () {},
        "_getEvent": function () {},
        "_filterClick": function () {},
        "on": function () {},
        "off": function () {},
        "_touchstart": {},
        "_touchend": {},
        "addDoubleTapListener": function () {},
        "removeDoubleTapListener": function () {},
        "POINTER_DOWN": {},
        "POINTER_MOVE": {},
        "POINTER_UP": {},
        "POINTER_CANCEL": {},
        "_pointers": function () {},
        "_pointerDocumentListener": {},
        "addPointerListener": function () {},
        "addPointerListenerStart": function () {},
        "addPointerListenerMove": function () {},
        "addPointerListenerEnd": function () {},
        "removePointerListener": function () {}
    },
    "Draggable": function () {},
    "Handler": function () {},
    "Control": function () {},
    "control": function () {},
    "PosAnimation": function () {}
}

var prototype = {
    "prototype": {
        "prototype": {
            "prototype": {
                "constructor": function () {},
                "addEventListener": function () {},
                "hasEventListeners": function () {},
                "removeEventListener": function () {},
                "clearAllEventListeners": function () {},
                "fireEvent": function () {},
                "addOneTimeEventListener": function () {},
                "on": function () {},
                "off": function () {},
                "once": function () {},
                "fire": function () {},
                "options": {
                    "crs": {
                        "latLngToPoint": function () {},
                        "pointToLatLng": function () {},
                        "project": function () {},
                        "scale": function () {},
                        "getSize": function () {},
                        "Simple": {
                            "latLngToPoint": function () {},
                            "pointToLatLng": function () {},
                            "project": function () {},
                            "scale": function () {},
                            "getSize": function () {},
                            "projection": {
                                "project": function () {},
                                "unproject": function () {}
                            },
                            "transformation": {
                                "_a": {},
                                "_b": {},
                                "_c": {},
                                "_d": {},
                                "transform": function () {},
                                "_transform": function () {},
                                "untransform": function () {}
                            }
                        },
                        "code": {},
                        "projection": {
                            "MAX_LATITUDE": {},
                            "project": function () {},
                            "unproject": function () {}
                        },
                        "transformation": {
                            "_a": {},
                            "_b": {},
                            "_c": {},
                            "_d": {},
                            "transform": function () {},
                            "_transform": function () {},
                            "untransform": function () {}
                        }
                    },
                    "fadeAnimation": {},
                    "trackResize": {},
                    "markerZoomAnimation": {},
                    "closePopupOnClick": {},
                    "dragging": {},
                    "inertia": {},
                    "inertiaDeceleration": {},
                    "inertiaMaxSpeed": {},
                    "inertiaThreshold": {},
                    "easeLinearity": {},
                    "worldCopyJump": {},
                    "doubleClickZoom": {},
                    "scrollWheelZoom": {},
                    "touchZoom": {},
                    "bounceAtZoomLimits": {},
                    "tap": {},
                    "tapTolerance": {},
                    "boxZoom": {},
                    "keyboard": {},
                    "keyboardPanOffset": {},
                    "keyboardZoomOffset": {},
                    "zoomControl": {},
                    "attributionControl": {},
                    "zoomAnimation": {},
                    "zoomAnimationThreshold": {}
                },
                "initialize": function () {},
                "setView": function () {},
                "setZoom": function () {},
                "zoomIn": function () {},
                "zoomOut": function () {},
                "setZoomAround": function () {},
                "fitBounds": function () {},
                "fitWorld": function () {},
                "panTo": function () {},
                "panBy": function () {},
                "setMaxBounds": function () {},
                "panInsideBounds": function () {},
                "addLayer": function () {},
                "removeLayer": function () {},
                "hasLayer": function () {},
                "eachLayer": function () {},
                "invalidateSize": function () {},
                "addHandler": function () {},
                "remove": function () {},
                "getCenter": function () {},
                "getZoom": function () {},
                "getBounds": function () {},
                "getMinZoom": function () {},
                "getMaxZoom": function () {},
                "getBoundsZoom": function () {},
                "getSize": function () {},
                "getPixelBounds": function () {},
                "getPixelOrigin": function () {},
                "getPanes": function () {},
                "getContainer": function () {},
                "getZoomScale": function () {},
                "getScaleZoom": function () {},
                "project": function () {},
                "unproject": function () {},
                "layerPointToLatLng": function () {},
                "latLngToLayerPoint": function () {},
                "containerPointToLayerPoint": function () {},
                "layerPointToContainerPoint": function () {},
                "containerPointToLatLng": function () {},
                "latLngToContainerPoint": function () {},
                "mouseEventToContainerPoint": function () {},
                "mouseEventToLayerPoint": function () {},
                "mouseEventToLatLng": function () {},
                "_initContainer": function () {},
                "_initLayout": function () {},
                "_initPanes": function () {},
                "_createPane": function () {},
                "_clearPanes": function () {},
                "_addLayers": function () {},
                "_resetView": function () {},
                "_rawPanBy": function () {},
                "_getZoomSpan": function () {},
                "_updateZoomLevels": function () {},
                "_panInsideMaxBounds": function () {},
                "_checkIfLoaded": function () {},
                "_initEvents": function () {},
                "_onResize": function () {},
                "_onMouseClick": function () {},
                "_fireMouseEvent": function () {},
                "_onTileLayerLoad": function () {},
                "_clearHandlers": function () {},
                "whenReady": function () {},
                "_layerAdd": function () {},
                "_getMapPanePos": function () {},
                "_moved": function () {},
                "_getTopLeftPoint": function () {},
                "_getNewTopLeftPoint": function () {},
                "_latLngToNewLayerPoint": function () {},
                "_getCenterLayerPoint": function () {},
                "_getCenterOffset": function () {},
                "_limitCenter": function () {},
                "_limitOffset": function () {},
                "_getBoundsOffset": function () {},
                "_rebound": function () {},
                "_limitZoom": function () {},
                "_initHooks": {
                    "0": function () {},
                    "1": function () {},
                    "2": function () {},
                    "3": function () {},
                    "4": function () {},
                    "5": function () {},
                    "6": function () {},
                    "7": function () {},
                    "8": function () {}
                },
                "callInitHooks": function () {},
                "openPopup": function () {},
                "closePopup": function () {},
                "_updatePathViewport": function () {},
                "_initPathRoot": function () {},
                "_animatePathZoom": function () {},
                "_endPathZoom": function () {},
                "_updateSvgViewport": function () {},
                "addControl": function () {},
                "removeControl": function () {},
                "_initControlPos": function () {},
                "_clearControlPos": function () {},
                "_onPanTransitionStep": function () {},
                "_onPanTransitionEnd": function () {},
                "_tryAnimatedPan": function () {},
                "_catchTransitionEnd": function () {},
                "_nothingToAnimate": function () {},
                "_tryAnimatedZoom": function () {},
                "_animateZoom": function () {},
                "_onZoomTransitionEnd": function () {},
                "_defaultLocateOptions": {
                    "watch": {},
                    "setView": {},
                    "maxZoom": {},
                    "timeout": {},
                    "maximumAge": {},
                    "enableHighAccuracy": {}
                },
                "locate": function () {},
                "stopLocate": function () {},
                "_handleGeolocationError": function () {},
                "_handleGeolocationResponse": function () {}
            }
        }
    }
}

var prototype = {
    "prototype": {
        "prototype": {
            "prototype": {
                "constructor": function () {},
                "addEventListener": function () {},
                "hasEventListeners": function () {},
                "removeEventListener": function () {},
                "clearAllEventListeners": function () {},
                "fireEvent": function () {},
                "addOneTimeEventListener": function () {},
                "on": function () {},
                "off": function () {},
                "once": function () {},
                "fire": function () {},
                "options": {
                    "minZoom": {},
                    "maxZoom": {},
                    "tileSize": {},
                    "subdomains": {},
                    "errorTileUrl": {},
                    "attribution": {},
                    "zoomOffset": {},
                    "opacity": {},
                    "unloadInvisibleTiles": {},
                    "updateWhenIdle": {}
                },
                "initialize": function () {},
                "onAdd": function () {},
                "addTo": function () {},
                "onRemove": function () {},
                "bringToFront": function () {},
                "bringToBack": function () {},
                "getAttribution": function () {},
                "getContainer": function () {},
                "setOpacity": function () {},
                "setZIndex": function () {},
                "setUrl": function () {},
                "redraw": function () {},
                "_updateZIndex": function () {},
                "_setAutoZIndex": function () {},
                "_updateOpacity": function () {},
                "_initContainer": function () {},
                "_reset": function () {},
                "_getTileSize": function () {},
                "_update": function () {},
                "_addTilesFromCenterOut": function () {},
                "_tileShouldBeLoaded": function () {},
                "_removeOtherTiles": function () {},
                "_removeTile": function () {},
                "_addTile": function () {},
                "_getZoomForUrl": function () {},
                "_getTilePos": function () {},
                "getTileUrl": function () {},
                "_getWrapTileNum": function () {},
                "_adjustTilePoint": function () {},
                "_getSubdomain": function () {},
                "_getTile": function () {},
                "_resetTile": function () {},
                "_createTile": function () {},
                "_loadTile": function () {},
                "_tileLoaded": function () {},
                "_tileOnLoad": function () {},
                "_tileOnError": function () {},
                "_initHooks": function () {},
                "callInitHooks": function () {},
                "_animateZoom": function () {},
                "_endZoomAnim": function () {},
                "_clearBgBuffer": function () {},
                "_prepareBgBuffer": function () {},
                "_getLoadedTilesPercentage": function () {},
                "_stopLoadingImages": function () {}
            }
        }
    }
}
var prototype = {
    "prototype": {
        "prototype": {
            "prototype": {
                "equals": function () {},
                "toString": function () {},
                "distanceTo": function () {},
                "wrap": function () {}
            }
        }
    }
}
// Add geocoder to externs
L.Control = {};
L.Control.geocoder = {};