(defproject sistr-app "0.2.0"
  :description "SISTR: RESTful client built using Clojurescript, Reagent (ReactJS) and D3.js"
  :url "http://bitbucket.org/peterk87/sistr-app/"
  :license {:name "Eclipse Public License"
            :url  "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [http-kit "2.2.0-alpha2"]
                 [org.clojure/clojurescript "1.9.36"]
                 [org.clojure/core.async "0.2.374"]
                 [lein-figwheel "0.5.4-2"]
                 [figwheel-sidecar "0.5.0-SNAPSHOT"]
                 [cljsjs/bootstrap "3.3.5-0"]
                 [cljsjs/selectize "0.12.1-1"]
                 [cljsjs/d3 "3.5.16-0"]
                 [cljsjs/dimple "2.1.2-0"]
                 [cljsjs/leaflet "0.7.7-4"]
                 [reagent "0.6.0-rc"]
                 [reagent-forms "0.5.24"]
                 [cljs-ajax "0.5.5"]
                 [inflections "0.12.2"]
                 [binaryage/devtools "0.7.2"]]

  :plugins [[lein-cljsbuild "1.1.3"]
            [lein-figwheel "0.5.4-2"]]

  :profiles {:dev {:dependencies [[com.cemerick/piggieback "0.2.1"]
                                  [org.clojure/tools.nrepl "0.2.11"]]
                   :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}}}

  :clean-targets ^{:protect false} ["resources/public/js/compiled/out" "resources/public/js/compiled"]
  :cljsbuild {
              :builds [{:id           "dev"
                        :source-paths ["src" "dev_src/cljs"]
                        :compiler     {:output-to            "resources/public/js/compiled/sistr_app.js"
                                       :output-dir           "resources/public/js/compiled/out"
                                       :main                 sistr-app.dev
                                       :asset-path           "js/compiled/out"
                                       :source-map           true
                                       :optimizations        :none
                                       :pretty-print         true
                                       :source-map-timestamp true
                                       :cache-analysis       true
                                       :libs                 ["resources/public/js/tree.js"
                                                              "resources/public/js/mst.js"
                                                              "resources/public/js/dynamite.js"
                                                              "resources/public/js/lib/slick.core.js"
                                                              "resources/public/js/lib/slick.dataview.js"
                                                              "resources/public/js/lib/slick.grid.js"
                                                              "resources/public/js/lib/slick-grid-plugins/slick.rowselectionmodel.js"
                                                              "resources/public/js/lib/slick-grid-controls/slick.columnpicker.js"
                                                              "resources/public/js/lib/slick-grid-plugins/slick.autotooltips.js"
                                                              "resources/public/js/lib/slick-grid-plugins/slick.checkboxselectcolumn.js"
                                                              "resources/public/js/lib/jquery.event.drag-2.2.js"
                                                              "resources/public/js/lib/jquery.event.drop-2.2.js"
                                                              "resources/public/js/lib/jquery-ui-1.11.4-custom.min.js"]
                                       }}

                       {:id           "prod"
                        :source-paths ["src"]
                        :compiler
                                      {:output-to     "resources/public/js/compiled/sistr_app.js"
                                       :optimizations :advanced
                                       :pretty-print  false
                                       :externs       ["externs/Leaflet_externs.js"
                                                       "externs/externs.js"
                                                       "resources/public/js/tree.js"
                                                       "resources/public/js/mst.js"
                                                       "resources/public/js/dynamite.js"
                                                       "resources/public/js/lib/slick.core.js"
                                                       "resources/public/js/lib/slick.dataview.js"
                                                       "resources/public/js/lib/slick.grid.js"
                                                       "resources/public/js/lib/slick-grid-plugins/slick.rowselectionmodel.js"
                                                       "resources/public/js/lib/slick-grid-controls/slick.columnpicker.js"
                                                       "resources/public/js/lib/slick-grid-plugins/slick.autotooltips.js"
                                                       "resources/public/js/lib/slick-grid-plugins/slick.checkboxselectcolumn.js"
                                                       "resources/public/js/lib/jquery.event.drag-2.2.js"
                                                       "resources/public/js/lib/jquery.event.drop-2.2.js"
                                                       "resources/public/js/lib/jquery-ui-1.11.4-custom.min.js"]
                                       :libs          ["resources/public/js/tree.js"
                                                       "resources/public/js/mst.js"
                                                       "resources/public/js/dynamite.js"
                                                       "resources/public/js/lib/slick.core.js"
                                                       "resources/public/js/lib/slick.dataview.js"
                                                       "resources/public/js/lib/slick.grid.js"
                                                       "resources/public/js/lib/slick-grid-plugins/slick.rowselectionmodel.js"
                                                       "resources/public/js/lib/slick-grid-controls/slick.columnpicker.js"
                                                       "resources/public/js/lib/slick-grid-plugins/slick.autotooltips.js"
                                                       "resources/public/js/lib/slick-grid-plugins/slick.checkboxselectcolumn.js"
                                                       "resources/public/js/lib/jquery.event.drag-2.2.js"
                                                       "resources/public/js/lib/jquery.event.drop-2.2.js"
                                                       "resources/public/js/lib/jquery-ui-1.11.4-custom.min.js"]}}]}

  :figwheel {:http-server-root "public"
             :server-port      3449
             :nrepl-port       7888
             :css-dirs         ["resources/public/css"]})
